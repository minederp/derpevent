///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package me.ctharvey.derpevents.event.objective.overall;
//
//import java.util.List;
//import me.ctharvey.derpevents.events.instance.config.points.Point;
//import org.bukkit.entity.Player;
//
///**
// *
// * @author thronecth
// */
//public interface OOInterface {
//
//    public void init();
//
//    public int getNumObjectivesLeft();
//
//    public String getName();
//
//    public boolean isCompleted();
//
//    public double getPercentComplete();
//
//    /**
//     * Returns a list of strings. Iterate through and each entry is a separate
//     * line.
//     *
//     * @return
//     */
//    public List<String> getLeaderBoard();
//
//    public List<Point> getPointsLeft();
//
//    public void addPlayerPoint(Player player, int pointAmount);
//
//    public String getWinner();
//
//    public boolean okToEnd();
//
//}
