///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package me.ctharvey.derpevents.event.objective.overall;
//
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.TreeMap;
//import me.ctharvey.Helper.Helper;
//import me.ctharvey.derpevents.events.instance.config.points.MonsterPoint;
//import me.ctharvey.derpevents.events.instance.config.points.Point;
//import me.ctharvey.derpevents.event.objective.ObjectiveController;
//import me.ctharvey.derpevents.event.objective.point.PointObjectiveInterface;
//import me.ctharvey.derpevents.event.objective.point.PointObjectiveInterface.PointObjectiveType;
//import org.bukkit.ChatColor;
//import org.bukkit.entity.Entity;
//import org.bukkit.entity.LivingEntity;
//import org.bukkit.entity.Player;
//
///**
// *
// * @author thronecth
// */
//public class KillMobsOverallObjective extends OverallObjective {
//
//    public KillMobsOverallObjective() {
//        this.type = ObjectiveController.ObjectiveType.KILLMOBS;
//    }
//
//    @Override
//    public int getNumObjectivesLeft() {
//        return this.pointsRemaining.size();
//    }
//
//    @Override
//    public String getName() {
//        return "PvE Top Killer";
//    }
//
//    @Override
//    public boolean isCompleted() {
//        if (this.pointsRemaining.isEmpty()) {
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public double getPercentComplete() {
//        if (container.getObjectiveMonsterPoints().isEmpty()) {
//            return 0;
//        }
//        return getPointsLeft().size() / container.getObjectiveMonsterPoints().size();
//    }
//
//    @Override
//    public List<String> getLeaderBoard() {
//        List<String> scores = new ArrayList();
//        scores.add("&6################################");
//        scores.add("&6############Scores##############");
//        ValueComparator bvc = new ValueComparator(this.scoreBoard);
//        TreeMap<String, Integer> sorted_map = new TreeMap<>(bvc);
//        for (Entry<String, Integer> entry : sorted_map.entrySet()) {
//            scores.add(entry.getKey() + ": " + entry.getValue());
//        }
//        scores.add("&6################################");
//        return scores;
//    }
//
//    @Override
//    public void addPlayerPoint(Player player, int pointAmount) {
//        if (this.scoreBoard.containsKey(player.getName())) {
//            int score = this.scoreBoard.get(player.getName()) + pointAmount;
//            this.scoreBoard.put(player.getName(), score);
//        } else {
//            this.scoreBoard.put(player.getName(), pointAmount);
//
//        }
//    }
//
//    @Override
//    public void init() {
//        for (MonsterPoint point : container.getMonsters()) {
//            if (point.getObjective().equals(PointObjectiveInterface.PointObjectiveType.KILLALLMOBS)) {
//                this.pointsRemaining.add(point);
//            }
//        }
//    }
//
//    @Override
//    public String getWinner() {
//        ValueComparator bvc = new ValueComparator(this.scoreBoard);
//        TreeMap<String, Integer> sorted_map = new TreeMap<>(bvc);
//        return sorted_map.firstKey();
//
//    }
//
//    @Override
//    public boolean okToEnd() {
//        if (getPointsLeft() == null) {
//            return false;
//        }
//        return getPointsLeft().isEmpty();
//    }
//
//    @Override
//    public List<Point> getPointsLeft() {
//        List<Point> pointsLeft = new ArrayList();
//        for (MonsterPoint point : this.container.getMonsters()) {
//            if (point.getObjective() != null && point.getObjective().equals(PointObjectiveType.KILLALLMOBS)) {
//                if (point.getAmountLeftToSpawn() > 0 || point.getCurrentMobs().size() > 0) {
//                    Helper.styleLog(point.toString());
//                    pointsLeft.add(point);
//                }
//            }
//        }
//        return pointsLeft;
//    }
//
//    class ValueComparator implements Comparator<String> {
//
//        Map<String, Integer> base;
//
//        public ValueComparator(Map<String, Integer> base) {
//            this.base = base;
//        }
//
//        // Note: this comparator imposes orderings that are inconsistent with equals.
//        @Override
//        public int compare(String a, String b) {
//            if (base.get(a) >= base.get(b)) {
//                return -1;
//            } else {
//                return 1;
//            } // returning 0 would merge keys
//        }
//    }
//}
