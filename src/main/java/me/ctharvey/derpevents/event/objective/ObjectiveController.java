///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package me.ctharvey.derpevents.event.objective;
//
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import me.ctharvey.derpevents.event.objective.overall.GetToLocationOverallObjective;
//import me.ctharvey.derpevents.event.objective.overall.GetTreasureOverallObjective;
//import me.ctharvey.derpevents.event.objective.overall.LastManStandingOO;
//import me.ctharvey.derpevents.event.objective.overall.KingOfTheHillOO;
//import me.ctharvey.derpevents.event.objective.overall.MostKillsInTimeOO;
//import me.ctharvey.derpevents.event.objective.overall.KillMobsOverallObjective;
//import me.ctharvey.derpevents.event.objective.overall.OverallObjective;
//
///**
// *
// * @author thronecth
// */
//public class ObjectiveController {
//
//    public static enum ObjectiveType{
//        KILLMOBS(KillMobsOverallObjective.class),
//        GETTOLOCATION(GetToLocationOverallObjective.class),
//        GETTREASURE(GetTreasureOverallObjective.class),
//        LASTMANSTANDING(LastManStandingOO.class),
//        MOSTKILLSINTIME(MostKillsInTimeOO.class),
//        KINGOFTHEHILL(KingOfTheHillOO.class);
//
//        public static ObjectiveType get(String string) {
//            for(ObjectiveType type: values()){
//                if(type.toString().equalsIgnoreCase(string)){
//                    return type;
//                }
//            }
//            return null;
//        }
//
//        private Class clazz;
//
//        private ObjectiveType(Class clazz) {
//            this.clazz = clazz;
//        }
//
//        public OverallObjective getObjectiveObject() {
//            try {
//                try {
//                    return (OverallObjective) Class.forName(clazz.getName()).newInstance();
//                } catch (ClassNotFoundException ex) {
//                    Logger.getLogger(ObjectiveController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            } catch (    InstantiationException | IllegalAccessException ex) {
//                Logger.getLogger(ObjectiveController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            return null;
//        }
//
//        public Class getClazz() {
//            return clazz;
//        }
//
//    }
//
//
//}
