/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevents.event.objective.point;

import org.bukkit.ChatColor;

/**
 *
 * @author thronecth
 */
public interface PointObjectiveInterface {

    public static enum PointObjectiveType {

        KILLALLMOBS,
        GETTOLOCATION,
        OPENTREASURECHEST,
        NONE;

        public static PointObjectiveType getType(String type) {
            for (PointObjectiveType aType : values()) {
                if (aType.toString().equalsIgnoreCase(type)) {
                    return aType;
                }
            }
            return NONE;

        }
    }

    public boolean objectiveObtained();

    public String getPlayerWhoObtained();

    public int percentageComplete();

    public class KillMobsObjective {

    }

    public class ReachLocationObjective {

    }

    public class OpenChestObjective {

    }

}
