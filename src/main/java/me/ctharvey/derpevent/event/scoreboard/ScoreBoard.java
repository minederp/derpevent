/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.scoreboard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import me.ctharvey.derpevent.event.Event;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class ScoreBoard {

    boolean scoreChanged = false;

    public List<Score> scores = new ArrayList();

    public Score addScore(Player player) {
        return addScore(player.getName());
    }

    public Score addScore(String name) {
        Score score1 = new Score(name, 0, 0, 0);
        scores.add(score1);
        scoreChanged = true;
        return score1;
    }

    public void removeScore(Player player) {
        removeScore(player.getName());
        scoreChanged = true;
    }

    public void removeScore(String name) {
        for (Iterator<Score> it = scores.iterator(); it.hasNext();) {
            Score score = it.next();
            if (score.getPlayerName().equals(name)) {
                it.remove();
                scoreChanged = true;
                break;
            }
        }
    }

    public List<Score> getScores() {
        Collections.sort(scores);
        return scores;
    }

    public void removePlayer(Player player) {
        for (Iterator<Score> it = scores.iterator(); it.hasNext();) {
            Score score = it.next();
            if (score.getPlayerName().equals(player.getName())) {
                it.remove();
                scoreChanged = true;
            }
        }
    }

    public boolean isScoreChanged() {
        return scoreChanged;
    }

}
