/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.scoreboard.damagetracker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import me.ctharvey.derpevent.event.Event;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class DamageTracker {

    private final LinkedList<DamageEntry> entries = new LinkedList<>();

    public void addEntry(DamageEntry entry) {
        entries.add(entry);
    }

    public void purgeOld() {
        for (Iterator<DamageEntry> it = entries.iterator(); it.hasNext();) {
            DamageEntry entry = it.next();
            if (entry.getTime() + 10 < TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) {
                it.remove();
            }
        }
    }

    public DamageEntry getNewest(Player playerDamaged) {
        String name = playerDamaged.getName();
        for (Iterator<DamageEntry> it = entries.descendingIterator(); it.hasNext();) {
            DamageEntry entry = it.next();
            if (entry.getDamagee().equals(name)) {
                return entry;
            }
        }
        return null;
    }

    public List<DamageEntry> getAllDamaging(Player playerDamaged) {
        List<DamageEntry> playersDamaging = new ArrayList();
        for (DamageEntry entry : entries) {
            if (entry.getDamagee().equals(playerDamaged.getName())) {
                playersDamaging.add(entry);
            }
        }
        return playersDamaging;
    }

    public List<Player> getAllDamagingPlayers(Player killer, Player player) {
        List<Player> damagers = new ArrayList();
        for (DamageEntry entry : this.getAllDamaging(player)) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(entry.getDamager());
            if (!damagers.contains(offlinePlayer.getPlayer())) {
                damagers.add(offlinePlayer.getPlayer());
            }
        }
        damagers.remove(killer);
        return damagers;
    }

    public static class DamageEntry {

        private final Integer time;
        private final String damager, damagee;

        public DamageEntry(Integer time, String damager, String damagee) {
            this.time = time;
            this.damager = damager;
            this.damagee = damagee;
        }

        public Integer getTime() {
            return time;
        }

        public String getDamager() {
            return damager;
        }

        public String getDamagee() {
            return damagee;
        }

    }
}
