/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.scoreboard;

/**
 *
 * @author thronecth
 */
public class Score implements Comparable<Score> {

    private final String playerName;
    private int points, kills, assists, death;

    public Score(String key, int value, int kills, int deaths) {
        this.playerName = key;
        this.points = value;
        this.kills = kills;
        this.death = deaths;
    }

    public String getPlayerName() {
        return playerName;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public void removePoints(int points) {
        this.points -= points;
    }

    public int getKills() {
        return kills;
    }

    @Override
    public int compareTo(Score t) {
        if (this.getPoints() == t.getPoints()) {
            if (this.getKills() == t.getKills()) {
                return t.getPlayerName().compareToIgnoreCase(t.getPlayerName());
            }
            if (this.getKills() > t.getKills()) {
                return -1;
            } else {
                return 1;
            }
        } else if (this.getPoints() > t.getPoints()) {
            return -1;
        } else {
            return 1;
        }
    }

    public void addKill() {
        kills++;
    }

    public void addAssist() {
        assists++;
    }

    public int getAssists() {
        return assists;
    }

    public void addDeath() {
        death++;
    }

    public int getDeath() {
        return death;
    }

}
