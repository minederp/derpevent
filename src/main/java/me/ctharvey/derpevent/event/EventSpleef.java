/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import java.util.Collections;
import me.spathwalker.realms.zone.Zone;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class EventSpleef extends EventLastManStanding {

    public EventSpleef(Player player, String name, World world, Zone zone) {
        super(player, name, world, zone);
        this.areaIsLocked = true;
        this.type = EventType.SPLEEF;
        this.blockTypeToBreak.add(Material.SNOW_BLOCK);
    }

    @Override
    public void startEvent() {
        super.startEvent(); //To change body of generated methods, choose Tools | Templates.
        this.zone.addFlag("B");
        this.zone.addFlag("D");
        zone.update();
    }

    @Override
    public void endEvent() {
        super.endEvent(); //To change body of generated methods, choose Tools | Templates.
        this.zone.removeFlag("B");
        this.zone.removeFlag("D");
        zone.update();
    }

}
