/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import com.adamantdreamer.foundation.ui.noteboard.Note;
import com.adamantdreamer.foundation.ui.noteboard.Note.Priority;
import com.adamantdreamer.foundation.ui.noteboard.Unique;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.derpevent.event.scoreboard.Score;
import me.ctharvey.derpevent.event.scoreboard.ScoreBoard;
import me.ctharvey.derpevent.event.scoreboard.damagetracker.DamageTracker;
import me.ctharvey.derpevent.event.scoreboard.damagetracker.DamageTracker.DamageEntry;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import me.ctharvey.derpevent.eventlisting.EventListing;
import me.ctharvey.derpevent.eventlisting.EventListingFactory;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory.InvalidQuormObjectException;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public abstract class Event implements IEvent {

    protected String name;

    protected List<Player> players = new ArrayList();
    protected Player owner;

    protected boolean pvpAllowed = true;
    protected boolean moveBeforeEvent = false;
    protected boolean spawnMobs = false;
    protected boolean spawnAnimals = false;
    protected boolean whitelistOn = false;

    /**
     * <Player name, score object> @return
     */
    protected HashMap<String, Score> score = new HashMap();

    protected final ScoreBoard scoreBoard = new ScoreBoard();
    protected final DamageTracker damageTracker = new DamageTracker();

    protected World world;

    protected Status status = Status.WAITING;

    protected EventType type;

    private static final Unique unique = new Unique(true);
    protected Note note;
    protected int timeRunning = 0;

    protected EventVariation.EquipmentOptions equipment;
    private EventListing eventListing;

    public final void initNewNote() {
        note = new com.adamantdreamer.foundation.ui.noteboard.Note("Derp Events", unique, Priority.CRITICAL, 10 * 60);
        setNoteText(note);
        note.show(owner);
    }

    public Event(Player player, String name, World world, EventType type) {
        this.name = StringUtils.capitalize(name.toLowerCase());
        this.world = world;
        this.type = type;
        this.owner = player;
        initNewNote();
        try {
            eventListing = new EventListing(status, this.name, Bukkit.getServerName(), type);
            EventListingFactory.getInstance().add(eventListing);
        } catch (SQLException | InvalidQuormObjectException ex) {
            Logger.getLogger(Event.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Status getStatus() {
        return status;
    }

    public EventType getType() {
        return type;
    }

    public void messagePlayers(String string) {
        for (Player player : getPlayers()) {
            DerpEvent.getPlugin().sendMsg(string, player);
        }
    }

    public void updateScoreBoard() {
        for (Player player : getPlayers()) {
            note.show(player);
        }
    }

    protected Note setNoteText(Note note) {
        note.addVar("time", ChatColor.RED + "Time Elapsed: " + ChatColor.GREEN + "{" + timeRunning + "s}");
        note.addVar("status", ChatColor.RED + "Status: " + ChatColor.BLUE + "{" + status.toString() + "}");
        return note;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public enum Status implements Serializable {

        WAITING,
        RUNNING,
        ENDED;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Player> getPlayers() {
        return players;
    }

    /**
     * <Player name, score object> @return
     *
     * @return
     */
    public HashMap<String, Score> getScore() {
        return score;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void endEvent() {
        try {
            this.status = Status.ENDED;
            EventListingFactory.getInstance().delete(eventListing);
            try {
                note.close();
            } catch (ConcurrentModificationException e) {
                for (Entry<String, Score> entry : getScore().entrySet()) {
                    Player player = Bukkit.getPlayer(entry.getKey());
                    player.getInventory().clear();
                    player.getInventory().setArmorContents(new ItemStack[player.getInventory().getArmorContents().length]);
                    if (player != null) {
                        note.setDuration(1);
                        note.hide(player);
                    }
                }
                note.hide(owner);
            }
            EventController.removeEvent(this);
        } catch (SQLException | InvalidQuormObjectException ex) {
            Logger.getLogger(Event.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Event.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ScoreBoard getScoreBoard() {
        return scoreBoard;
    }

    public void startEvent() {
        if (!players.isEmpty()) {
            for (Player player : getPlayers()) {
                DerpEvent.send(player, ChatColor.RED + name + ChatColor.GREEN + " is beginning!  Total players: " + ChatColor.BLUE + getPlayers().size());
            }
        }
        this.status = Status.RUNNING;
    }

    public void addPoints(Player player, int points) {
        if (!this.getScore().containsKey(player.getName())) {
            this.getScore().put(player.getName(), getScoreBoard().addScore(player));
        }
        this.getScore().get(player.getName()).addPoints(points);
    }

    public boolean addPlayer(Player player, Boolean whitelistBypass) {
        if (!whitelistOn || whitelistBypass) {
            if (getStatus().equals(Status.WAITING)) {
                if (!this.getPlayers().contains(player)) {
                    if (!this.getScore().containsKey(player.getName())) {
                        this.getScore().put(player.getName(), getScoreBoard().addScore(player));
                    }
                    messagePlayers(ChatColor.RED + player.getName() + ChatColor.RESET + " has joined the event.");
                    this.players.add(player);
                    this.note.show(player);
                    DerpEvent.send(player, "You have joined the event named " + ChatColor.RED + name + "&f It is a &b" + type.toString().toLowerCase().replace("_", " ") + "&f game type.");
                    player.getInventory().clear();
                    player.getInventory().setArmorContents(new ItemStack[player.getInventory().getArmorContents().length]);
                    return true;
                } else {
                    DerpEvent.send(player, "You are already in this event.");
                }
            } else {
                DerpEvent.send(player, "This event is already in progress or completed.");
            }
        } else {
            DerpEvent.send(player, "This event is whitelisted and requires a mod to add you manually.");
        }
        return false;
    }

    public void removePoints(Player player, int points) {
        if (!this.getScore().containsKey(player.getName())) {
            this.getScore().put(player.getName(), getScoreBoard().addScore(player));
        }
        this.getScore().get(player.getName()).removePoints(points);
    }

    public Score getWinner() {
        Score score = null;
        Collections.sort(scoreBoard.scores);
        if (!scoreBoard.scores.isEmpty()) {
            score = getScoreBoard().getScores().get(0);
        }
        return score;
    }

    @Override
    public void playerDied(final Player player, EntityDamageEvent cause, boolean forciblyRemove) {
        if (cause == null) {
            if (status != Status.RUNNING) {
                player.getInventory().clear();
                player.getInventory().setArmorContents(new ItemStack[player.getInventory().getArmorContents().length]);
                this.getPlayers().remove(player);
                this.score.remove(player.getName());
                this.scoreBoard.removePlayer(player);
            }
        } else if (cause.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK) && player.getKiller() != null) {
            playerAssistedKill(player.getKiller(), player, this.damageTracker.getAllDamagingPlayers(player.getKiller(), player));
        } else if (cause.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE) && cause instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) cause;
            if (event.getDamager() instanceof Projectile) {
                if (((Projectile) event.getDamager()).getShooter() instanceof Player) {
                    playerAssistedKill((Player) ((Projectile) event.getDamager()).getShooter(), player, this.damageTracker.getAllDamagingPlayers((Player) ((Projectile) event.getDamager()).getShooter(), player));
                }
            }
        } else {
            this.damageTracker.purgeOld();
            DamageEntry newest = this.damageTracker.getNewest(player);
            if (newest == null && status.equals(Status.RUNNING)) {
                messagePlayers(ChatColor.RED + player.getName() + " has died with no killer.");
            } else if (newest == null) {
            } else {
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(newest.getDamager());
                playerAssistedKill(offlinePlayer.getPlayer(), player, this.damageTracker.getAllDamagingPlayers(offlinePlayer.getPlayer(), player));
            }
        }
        Bukkit.getScheduler().runTaskLater(DerpEvent.getPlugin(), new Runnable() {

            @Override
            public void run() {
                player.setFireTicks(0);
            }
        }, 1);
    }

    public boolean isPvpAllowed() {
        return pvpAllowed;
    }

    public void setPvpAllowed(boolean pvpAllowed) {
        this.pvpAllowed = pvpAllowed;
    }

    @Override
    public void playerDied(Player player, EntityDamageEvent cause) {
        playerDied(player, cause, false);
    }

    public boolean scoreChanged() {
        return scoreBoard.isScoreChanged();
    }

    protected String getAssistString(List<Player> assistors) {
        if (assistors.size() == 1) {
            return assistors.get(0).getName();
        }
        StringBuilder sb = new StringBuilder();
        int cnt = 0;
        for (Player player : assistors) {
            cnt++;
            if (cnt == assistors.size()) {
                sb.append(" and ").append(player.getName());
            } else {
                sb.append(player.getName()).append(", ");
            }
        }
        return sb.toString();
    }

    public DamageTracker getDamageTracker() {
        return damageTracker;
    }

    public void setMoveBeforeEvent(boolean moveBeforeEvent) {
        this.moveBeforeEvent = moveBeforeEvent;
    }

    public boolean isSpawnMobs() {
        return spawnMobs;
    }

    public void setSpawnMobs(boolean spawnMobs) {
        this.spawnMobs = spawnMobs;
    }

    public boolean isSpawnAnimals() {
        return spawnAnimals;
    }

    public void setSpawnAnimals(boolean spawnAnimals) {
        this.spawnAnimals = spawnAnimals;
    }

    public boolean isWhitelistOn() {
        return whitelistOn;
    }

    public void setWhitelistOn(boolean whitelistOn) {
        this.whitelistOn = whitelistOn;
    }

    public EventVariation.EquipmentOptions getEquipment() {
        return equipment;
    }

    public void setEquipment(EventVariation.EquipmentOptions equipment) {
        this.equipment = equipment;
    }

}
