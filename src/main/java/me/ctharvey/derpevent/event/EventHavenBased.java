/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import me.spathwalker.realms.zone.Zone;
import me.spathwalker.realms.zone.Zones;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public abstract class EventHavenBased extends Event {

    protected Zone zone;
    private static Random rand = new Random();
    protected int currentSpawnIndex;
    protected List<Location> spawnLocations = new ArrayList();
    protected boolean areaIsLocked = true;
    protected Set<Material> blockTypeToBreak = new HashSet();
    protected Set<Material> blockTypeToPlace = new HashSet();

    public EventHavenBased(Player player, String name, World world, EventType type, Zone zone) {
        super(player, name, world, type);
        this.zone = zone;
    }

    public Zone getZone() {
        return zone;
    }

    protected void spawnLocations() {
        spawnLocations = new ArrayList();
        int[] contents = Zones.getChildren(zone.id);
        for (Integer i : contents) {
            Zone plot = Zones.load(i);
            int x;
            if (plot.box.getX1() < plot.box.getX2()) {
                x = plot.box.getX1() + rand.nextInt(Math.abs(plot.box.getX2() - plot.box.getX1()));
            } else {
                x = plot.box.getX2() + rand.nextInt(Math.abs(plot.box.getX1() - plot.box.getX2()));
            }
            int z;
            if (plot.box.getZ1() < plot.box.getZ2()) {
                z = plot.box.getZ1() + rand.nextInt(Math.abs(plot.box.getX2() - plot.box.getX1()));
            } else {
                z = plot.box.getZ2() + rand.nextInt(Math.abs(plot.box.getX1() - plot.box.getX2()));
            }
            Location loc = new Location(plot.box.getWorld(), x, plot.box.getMidY(), z);
            spawnLocations.add(loc);
        }
        if (spawnLocations.isEmpty()) {
            spawnLocations = null;
        }
    }

    public Location getNextSpawnLocation() {
        if (spawnLocations != null && spawnLocations.isEmpty()) {
            spawnLocations = null;
        }
        currentSpawnIndex++;
        if (spawnLocations == null || currentSpawnIndex > spawnLocations.size() - 1) {
            spawnLocations();
            currentSpawnIndex = 0;
        }
        if (spawnLocations.isEmpty()) {
            spawnLocations = null;
            currentSpawnIndex = 0;
            return null;
        }
        return spawnLocations.get(currentSpawnIndex);
    }

    @Override
    public boolean addPlayer(Player player, Boolean whitelistBypass) {
        if (super.addPlayer(player, whitelistBypass)) {
            Location nextSpawnLocation = getNextSpawnLocation();
            if (nextSpawnLocation != null) {
                player.teleport(nextSpawnLocation);
                return true;
            } else {
                Location randomSpawnLocation = getRandomSpawnLocation();
                if (randomSpawnLocation != null) {
                    player.teleport(randomSpawnLocation);
                    return true;
                } else {
                    ((IEvent) this).playerDied(player, null, true);
                }
            }
        }
        return false;
    }

    protected Location getRandomSpawnLocation() {
        Zone plot = zone;
        int x, y, z;
        if (plot.box.getX1() < plot.box.getX2()) {
            x = plot.box.getX1() + rand.nextInt(Math.abs(plot.box.getX2() - plot.box.getX1()));
        } else {
            x = plot.box.getX2() + rand.nextInt(Math.abs(plot.box.getX1() - plot.box.getX2()));
        }
        if (plot.box.getZ1() < plot.box.getZ2()) {
            z = plot.box.getZ1() + rand.nextInt(Math.abs(plot.box.getZ2() - plot.box.getZ1()));
        } else {
            z = plot.box.getZ2() + rand.nextInt(Math.abs(plot.box.getZ1() - plot.box.getZ2()));
        }
        y = plot.box.getMidY();
        Location loc = new Location(world, x, y, z);
        if (!loc.getBlock().getType().equals(Material.AIR)) {
            loc = getSafeBlock(loc);
        }
        return loc;
    }

    private Location getSafeBlock(Location loc) {
        for (int x = -6; x < 12; x++) {
            loc = loc.subtract(0, x, 0);
            if (loc.getBlock().getType().equals(Material.AIR)) {
                return loc;
            }
        }
        return null;
    }

    public boolean isAreaLocked() {
        return areaIsLocked;
    }

    public boolean allowsBlockBreak(Material type) {
        return this.blockTypeToBreak.contains(type);
    }

    @Override
    public void endEvent() {
        super.endEvent(); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean allowsBlockPlace(Material type) {
        return this.blockTypeToPlace.contains(type);
    }

    public void removeAllowedToBreakBlock(Material type) {
        this.blockTypeToBreak.remove(type);
    }

    public void removeAllowedToPlaceBlock(Material type) {
        this.blockTypeToPlace.remove(type);
    }

    public void addAllowedToBreakBlocks(Material type) {
        this.blockTypeToBreak.add(type);
    }

    public void addAllowedToPlaceBlocks(Material type) {
        this.blockTypeToPlace.add(type);
    }

}
