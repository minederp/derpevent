/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import me.spathwalker.realms.zone.Zone;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public enum EventType {

    SINGLE_LIFE(
            true,
            Material.APPLE,
            new String[]{"Single Life", "Same as Last"}),
    TIMED(
            true,
            Material.COMPASS,
            new String[]{"PVP Timed.", "Kills/Assists earn points."}),
    LAST_MAN_STANDING(
            true,
            Material.APPLE,
            new String[]{"Single Life", "Last Man Standing wins"}),
    SPLEEF(
            true,
            Material.SNOW_BALL,
            new String[]{"Standard spleef rules."}),
    TEAM(
            false,
            Material.DIAMOND,
            new String[]{"Team based PVP"});

    private final boolean requiresHaven;
    private final Material icon;
    private final String[] lore;

    private EventType(boolean requiresHaven, Material icon, String[] lore) {
        this.requiresHaven = requiresHaven;
        this.icon = icon;
        this.lore = lore;
    }

    public static EventType get(String name) {
        switch (name.toLowerCase()) {
            case "single":
            case "singlelife":
            case "singe_life":
                return SINGLE_LIFE;
            case "timed":
                return TIMED;
            case "last":
            case "lastman":
            case "last_man":
            case "lastmanstanding":
            case "last_man_standing":
                return LAST_MAN_STANDING;
            case "team":
                return TEAM;
            case "spleef":
                return SPLEEF;
        }
        return null;
    }

    public static String getStringList() {
        StringBuilder sb = new StringBuilder();
        for (EventType type : values()) {
            sb.append(type.toString().toLowerCase()).append(", ");
        }
        return sb.toString().substring(0, sb.length() - 1);
    }

    public boolean requiresHaven() {
        return requiresHaven;
    }

    public Event createType(Player owner, String name, World world, Zone zone) {
        switch (this) {
            case LAST_MAN_STANDING:
            case SINGLE_LIFE:
                return new EventLastManStanding(owner, name, world, zone);
            case SPLEEF:
                return new EventSpleef(owner, name, world, zone);
            case TEAM:
                return null;
            case TIMED:
                return new EventTimed(owner, name, world, zone);
        }
        return null;
    }

    public Material getIcon() {
        return this.icon;
    }

    public String[] getLore() {
        return lore;
    }

}
