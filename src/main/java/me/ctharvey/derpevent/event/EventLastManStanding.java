/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import com.adamantdreamer.foundation.ui.noteboard.Note;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.event.scoreboard.Score;
import me.spathwalker.realms.zone.Zone;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class EventLastManStanding extends EventHavenBased {

    protected List<String> deadPlayers = new ArrayList<>();

    public EventLastManStanding(Player player, String name, World world, Zone zone) {
        super(player, name, world, EventType.LAST_MAN_STANDING, zone);
    }

    @Override
    public void playerJoined(Player player) {
        this.players.add(player);
    }

    public List<String> getDeadPlayers() {
        return deadPlayers;
    }

    public void playerKilledPlayer(Player killer, Player killed) {
        messagePlayers(killer.getName() + " has eliminated " + killed.getName());
        score.get(killer.getName()).addKill();
    }

    @Override
    public boolean okToEnd() {
        return getPlayers().isEmpty() || getPlayers().size() < 2;
    }

    @Override
    public boolean okToStart() {
        return getPlayers().size() > 0;
    }

    @Override
    public void endEvent() {
        if (getWinner() != null && Bukkit.getPlayer(getWinner().getPlayerName()) != null) {
            String msg = ChatColor.translateAlternateColorCodes('&', "The event &c" + getName() + "&r has ended.  &b" + getWinner().getPlayerName() + "&r won with &c" + getWinner().getPoints() + "&r points.");
            DerpEvent.getPlugin().broadcast(msg);
        }
        super.endEvent();
    }

    @Override
    public void playerDied(Player player, EntityDamageEvent cause, boolean forciblyRemove) {
        super.playerDied(player, cause, forciblyRemove);
        this.players.remove(player);
        if (status.equals(Status.RUNNING)) {
            this.deadPlayers.add(player.getName());
            this.removePoints(player, 1);
            player.getInventory().clear();
            player.getInventory().setArmorContents(new ItemStack[player.getInventory().getArmorContents().length]);
        }
        player.teleport(player.getWorld().getSpawnLocation());
    }

    List<DamageCause> causeDisallowedAssists = Arrays.asList(DamageCause.CUSTOM, DamageCause.VOID);

    @Override
    public void playerAssistedKill(Player killer, Player killed, List<Player> assistors) {
        for (Player assistor : assistors) {
            score.get(assistor.getName()).addAssist();
        }
        playerKilledPlayer(killer, killed);
        if (assistors.isEmpty()) {
            messagePlayers(ChatColor.RED + killer.getName() + ChatColor.WHITE + " has killed " + ChatColor.BLUE + killed.getName());
        } else {
            messagePlayers(ChatColor.RED + killer.getName() + ChatColor.WHITE + " has killed " + ChatColor.BLUE + killed.getName() + ChatColor.WHITE + " with assistance from " + getAssistString(assistors));
        }
    }

    @Override
    public void updateScoreBoard() {
        super.updateScoreBoard();
        if (status.equals(Status.RUNNING)) {
            timeRunning++;
            note.setVar("time", timeRunning + "s");
        }
        note.setVar("status", "    " + status.toString() + ChatColor.WHITE + "{" + ChatColor.RED + getPlayers().size() + ChatColor.WHITE + "}");
        if (scoreChanged()) {
            Collections.sort(scoreBoard.scores);
            if (!scoreBoard.getScores().isEmpty()) {
                for (int x = 0; x < scoreBoard.getScores().size() && x < 10; x++) {
                    Score get = scoreBoard.getScores().get(x);
                    if (get.getPoints() > -1) {
                        note.set(x + 3, ChatColor.GREEN + get.getPlayerName());
                    } else {
                        note.set(x + 3, ChatColor.GRAY + get.getPlayerName());

                    }
                }
            }
        }
    }

    @Override
    protected Note setNoteText(Note note) {
        super.setNoteText(note);
        note.add("Remaining Players:");
        return note;
    }

}
