/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.points;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import me.ctharvey.dataobjects.SerializableLocation;
import org.bukkit.Location;

/**
 *
 * @author thronecth
 */
public class SpawnPoint extends Point {

    public SpawnPoint(Location loc) {
        super(new SerializableLocation(loc), PointType.SPAWNPOINT);
    }

    public SpawnPoint(Map<String, Object> map) {
        super((SerializableLocation) map.get("location"), PointType.SPAWNPOINT);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("location", location);
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SpawnPoint) {
            SpawnPoint point = (SpawnPoint) o;
            if (point.getLocation().equals(this.getLocation())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.location);
        return hash;
    }

}
