/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.points;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.UniqueMobs.UniqueMobs;
import me.ctharvey.dataobjects.SerializableLocation;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevents.event.objective.point.PointObjectiveInterface;
import me.ctharvey.derpevents.event.objective.point.PointObjectiveInterface.PointObjectiveType;
import me.ctharvey.uniquemobs2.MobData.Rarity;
import me.ctharvey.uniquemobs2.MobData.Template;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World.Environment;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Skeleton;

/**
 *
 * @author thronecth
 */
public final class MonsterPoint extends Point implements ConfigurationSerializable {

    private Rarity rarity;
    private final EntityType entityType;
    private int template = -1;
    private int amountToSpawn = 1;
    private int totalAmount = 1;
    private int currentAmountLeft = totalAmount;
    private Environment enviro;
    private List<LivingEntity> currentMobs = new ArrayList();
    private PointObjectiveType pointType;

    public MonsterPoint(Map<String, Object> map) {
        super((SerializableLocation) map.get("location"), PointType.MONSTERPOINT);
        this.rarity = Rarity.valueOf((String) map.get("priority"));
        this.entityType = EntityType.valueOf((String) map.get("type"));
        this.amountToSpawn = (int) map.get("amountToSpawn");
        this.totalAmount = (int) map.get("total_amount");
        this.enviro = Environment.valueOf((String) map.get("enviro"));
        PointObjectiveType type1 = PointObjectiveType.getType((String) map.get("point_type"));
        DerpEvent.getPlugin().log(type1);
        setPointType(type1);
        if (map.get("template") != null) {
            this.template = (int) map.get("template");
        }
    }

    public MonsterPoint(Location location, Rarity rarity, EntityType type) {
        super(new SerializableLocation(location), PointType.MONSTERPOINT);
        this.rarity = rarity;
        this.entityType = type;
        this.enviro = location.getWorld().getEnvironment();
        setPointType(PointObjectiveType.NONE);

    }

    public MonsterPoint(Location location, Rarity rarity, Template template) {
        super(new SerializableLocation(location), PointType.MONSTERPOINT);
        this.rarity = rarity;
        this.entityType = template.getType();
        this.template = template.getId();
        this.enviro = location.getWorld().getEnvironment();
        setPointType(PointObjectiveType.NONE);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap();
        map.put("priority", this.rarity.toString());
        map.put("location", this.location);
        map.put("type", this.entityType.toString());
        map.put("amountToSpawn", this.amountToSpawn);
        map.put("total_amount", this.totalAmount);
        map.put("point_type", getPointType().toString());
        if (this.enviro == null) {
            this.enviro = Environment.NORMAL;
        }
        map.put("enviro", this.enviro.toString());
        if (this.template != -1) {
            map.put("template", this.template);
        } else {
            map.put("template", -1);
        }
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MonsterPoint) {
            MonsterPoint point = (MonsterPoint) o;
            if (point.getPriority().equals(this.getPriority())) {
                if (point.getLocation().equals(this.getLocation())) {
                    if (point.getType().equals(this.getType())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public Rarity getPriority() {
        return rarity;
    }

    public void generateMob() {
        EntityType ent = entityType;
        for (int x = 0; x < this.amountToSpawn; x++) {
            if (getCurrentMobs().size() < amountToSpawn) {
                DerpEvent.getPlugin().log(entityType.toString() + " " + amountToSpawn);
                if (currentAmountLeft > 0) {
                    LivingEntity e = (LivingEntity) location.getLocation().getWorld().spawnEntity(location.getLocation().add(0, 2, 0), entityType);
                    addMob(e);
                    if (template != -1) {
                        getTemplate().spawnMob(e, true);
                    } else {
                        generateWildEntity(e, ent);
                    }
                    currentAmountLeft = currentAmountLeft - 1;
                    DerpEvent.getPlugin().log(currentAmountLeft);
                }
            }
        }
    }

    public void removeMob(LivingEntity monster) {
        getCurrentMobs().remove(monster);
    }

    public void generateWildEntity(LivingEntity e, EntityType ent) {
        if (ent == EntityType.SKELETON) {
            Skeleton es = (Skeleton) e;
            if (es.getSkeletonType().equals(Skeleton.SkeletonType.WITHER)) {
                ent = EntityType.WITHER_SKULL;
            }
        }
        Template generateTemplate = UniqueMobs.getPlugin().getController().getTemplateFactory().generateTemplate(rarity, enviro, ent);
        generateTemplate.spawnMob(e, true);
    }

    public int getAmountToSpawn() {
        return amountToSpawn;
    }

    public void setAmountToSpawnAtOnce(int initialAmount) {
        this.amountToSpawn = initialAmount;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
        this.currentAmountLeft = totalAmount;
    }

    public EntityType getType() {
        return entityType;
    }

    public List<LivingEntity> getCurrentMobs() {
        return currentMobs;
    }

    public Template getTemplate() {
        try {
            return UniqueMobs.getPlugin().getController().getTemplateFactory().getTemplate(template);
        } catch (SQLException ex) {
            Logger.getLogger(MonsterPoint.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Environment getEnvironment() {
        return this.enviro;
    }

    public PointObjectiveInterface.PointObjectiveType getObjective() {
        return pointType;
    }

    public void setPointType(PointObjectiveType type) {
        this.pointType = type;
        DerpEvent.getPlugin().log(type);
    }

    public void addMob(LivingEntity e) {
        getCurrentMobs().add(e);
    }

    public boolean isEmpty() {
        return getCurrentMobs().isEmpty() && getCurrentMobs() != null;
    }

    public void clearMobs() {
        List<LivingEntity> list = new ArrayList();
        for (LivingEntity ent : getCurrentMobs()) {
            ent.remove();
            list.add(ent);
        }
        getCurrentMobs().removeAll(list);
    }

    public int getAmountLeftToSpawn() {
        return this.currentAmountLeft;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String name = "";
        if (template != -1) {
            name = getTemplate().getName();
        }
        sb.append("MP-- Name: ").append(name).append(" Type: ").append(getType()).append(" TAmount: ").append(this.getTotalAmount()).append(" AmountLeft: ").append(this.currentAmountLeft);
        sb.append(" Currently: ").append(this.currentMobs.size()).append(" Object: ").append(getObjective());
        return sb.toString();
    }

    private Object getPointType() {
        return this.pointType;
    }

}
