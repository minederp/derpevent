/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.points;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.dataobjects.SerializableLocation;
import me.ctharvey.derpevent.DerpEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public final class TreasurePoint extends Point {

    List<DerpEventInventory> chestContents = new ArrayList();
    private int activeSlot = 0;

    public TreasurePoint(Map<String, Object> map) {
        super((SerializableLocation) map.get("location"), PointType.TREASUREPOINT);
        this.activeSlot = (int) map.get("active_slot");
        this.chestContents = (List<DerpEventInventory>) map.get("chest_contents");
    }

    public TreasurePoint(Location location, Inventory inventory, int chance) {
        super(new SerializableLocation(location), PointType.TREASUREPOINT);
        if (inventory == null) {
            Location loc = getLocation().getLocation();
            if (!loc.getBlock().getType().equals(Material.CHEST)) {
                loc.getBlock().setType(Material.CHEST);
                Chest chest = (Chest) loc.getBlock().getState();
                inventory = chest.getInventory();

            }
        }
        if (inventory == null) {
            inventory = Bukkit.createInventory(null, InventoryType.CHEST);
        }
        if (inventory == null) {
            DerpEvent.getPlugin().log("INVENTORY IS NULL?");
        }
        chestContents.add(new DerpEventInventory(chance, inventory));
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap();
        map.put("location", this.location);
        map.put("active_slot", this.activeSlot);
        map.put("chest_contents", this.chestContents);
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TreasurePoint) {
            TreasurePoint point = (TreasurePoint) o;
            if (point.getLocation().equals(this.getLocation())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    public void generateChest() {
        Location loc = getLocation().getLocation();
        if (!loc.getBlock().getType().equals(Material.CHEST)) {
            loc.getBlock().setType(Material.CHEST);
        }
        Chest chest = (Chest) loc.getBlock().getState();
        chest.getInventory().setContents(chestContents.get(activeSlot).getContents());
    }

    public void addItem(ItemStack is, int slot) {
        if (chestContents.get(slot) == null) {
            chestContents.add(slot, new DerpEventInventory(slot, Bukkit.createInventory(null, InventoryType.CHEST)));
        }
        chestContents.get(slot).getInventory().addItem(is);
    }

    public void removeItem(ItemStack is, int slot, int invSlot) {
        Inventory get = chestContents.get(slot).getInventory();
        get.setItem(invSlot, is);
    }

    public List<DerpEventInventory> getChestContents() {
        return chestContents;
    }

    public DerpEventInventory getCurrentActiveInventory() {
        return chestContents.get(activeSlot);
    }

    public int getActiveSlot() {
        return this.activeSlot;
    }

    public void setActiveSlot(Integer slot) {
        this.activeSlot = slot;
        try {
            this.chestContents.get(slot);
        } catch (IndexOutOfBoundsException ex) {
            this.chestContents.add(slot, new DerpEventInventory(50, Bukkit.createInventory(null, InventoryType.CHEST)));
        }
    }

    public static class DerpEventInventory implements ConfigurationSerializable {

        private final int numChance;
        private final Inventory inventory;

        public DerpEventInventory(int numChance, Inventory inventory) {
            this.numChance = numChance;
            this.inventory = inventory;
            if (this.inventory == null) {
                DerpEvent.getPlugin().log("WHY FOR YOU NULL");
            }
        }

        public DerpEventInventory(Map<String, Object> map) {
            this.numChance = (int) map.get("num_chance");
            this.inventory = Bukkit.createInventory(null, InventoryType.CHEST);
            List<ItemStack> itemStacks = (List<ItemStack>) map.get("inventory");
            this.inventory.setContents(itemStacks.toArray(new ItemStack[0]));
        }

        public int getNumChance() {
            return numChance;
        }

        public Inventory getInventory() {
            return inventory;
        }

        public ItemStack[] getContents() {
            return inventory.getContents();
        }

        @Override
        public Map<String, Object> serialize() {
            Map<String, Object> map = new HashMap();
            try {
                map.put("num_chance", this.numChance);
                if (this.inventory == null) {
                    throw new Exception("Inventory can't be null.");
                }
                map.put("inventory", this.inventory.getContents());
            } catch (Exception ex) {
                Logger.getLogger(TreasurePoint.class.getName()).log(Level.SEVERE, null, ex);
            }
            return map;
        }

    }
}
