/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event.points;

import java.util.Map;
import me.ctharvey.dataobjects.SerializableLocation;
import me.ctharvey.derpevents.event.objective.point.PointObjective;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

/**
 *
 * @author thronecth
 */
public abstract class Point implements ConfigurationSerializable {

    public static enum PointType {

        SPAWNPOINT,
        TREASUREPOINT,
        MONSTERPOINT,
        LOCATIONPOINT;
    }

    protected SerializableLocation location;
    protected PointObjective objective;
    protected PointType type;

    public Point(SerializableLocation location, PointType type) {
        this.location = location;
        this.type = type;
    }

    @Override
    public abstract Map<String, Object> serialize();

    public SerializableLocation getLocation() {
        return location;
    }

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract int hashCode();

}
