/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import com.adamantdreamer.foundation.ui.noteboard.Note;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.event.scoreboard.Score;
import me.spathwalker.realms.zone.Zone;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class EventTimed extends EventHavenBased {

    private int length = 60;
    private int startTime = 0;

    public EventTimed(Player player, String name, World world, Zone zone) {
        super(player, name, world, EventType.TIMED, zone);
    }

    public EventTimed(Player player, String name, World world, Zone zone, int length) {
        super(player, name, world, EventType.TIMED, zone);
        this.length = length;
    }

    public void playerKilledPlayer(Player killer, Player killed) {
        score.get(killer.getName()).addKill();
        score.get(killer.getName()).addPoints(5);
        score.get(killed.getName()).addDeath();
    }

    @Override
    public void playerAssistedKill(Player killer, Player killed, List<Player> assistors) {
        for (Player assistor : assistors) {
            score.get(assistor.getName()).addAssist();
            score.get(assistor.getName()).addPoints(1);
        }
        playerKilledPlayer(killer, killed);
        if (assistors.isEmpty()) {
            messagePlayers(ChatColor.RED + killer.getName() + ChatColor.WHITE + " has killed " + ChatColor.BLUE + killed.getName());
        } else {
            messagePlayers(ChatColor.RED + killer.getName() + ChatColor.WHITE + " has killed " + ChatColor.BLUE + killed.getName() + ChatColor.WHITE + " with assistance from " + getAssistString(assistors));
        }
    }

    @Override
    public void startEvent() {
        super.startEvent(); //To change body of generated methods, choose Tools | Templates.
        this.startTime = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }

    @Override
    public boolean okToEnd() {
        int current = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        return current > startTime + length;
    }

    @Override
    public boolean okToStart() {
        return this.getPlayers().size() > 0 && length > 0;
    }

    @Override
    public void playerJoined(Player player) {
        this.players.add(player);
    }

    @Override
    public void playerDied(Player player, EntityDamageEvent cause, boolean forciblyRemove) {
        super.playerDied(player, cause, forciblyRemove);
        Location nextSpawnLocation = getNextSpawnLocation();
        if (nextSpawnLocation != null) {
            player.teleport(nextSpawnLocation);
        } else {
            Location randomSpawnLocation = getRandomSpawnLocation();
            if (randomSpawnLocation != null) {
                player.teleport(randomSpawnLocation);
                player.setFireTicks(0);
            } else {
                ((IEvent) this).playerDied(player, null, true);
            }
        }
    }

    @Override
    public void endEvent() {
        if (getWinner() != null && Bukkit.getPlayer(getWinner().getPlayerName()) != null) {
            String msg = ChatColor.translateAlternateColorCodes('&', "The event &c" + getName() + "&r has ended.  &b" + getWinner().getPlayerName() + "&r won with &c" + getWinner().getPoints() + "&r points.");
            DerpEvent.getPlugin().broadcast(msg);
        }
        for (Player player : players) {
            player.getInventory().clear();
            player.getInventory().setArmorContents(new ItemStack[player.getInventory().getArmorContents().length]);
            player.teleport(player.getWorld().getSpawnLocation());
        }
        super.endEvent();
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    @Override
    public void updateScoreBoard() {
        super.updateScoreBoard();
        int current = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        int timeRemaining = startTime + length - current;
        if (timeRemaining < 0) {
            timeRemaining = length;
        }
        note.setVar("time", timeRemaining + "s");
        note.setVar("status", status.toString());
        Collections.sort(scoreBoard.scores);
        if (!scoreBoard.getScores().isEmpty()) {
            for (int x = 0; x < scoreBoard.getScores().size(); x++) {
                Score score = scoreBoard.getScores().get(x);
                String pName = score.getPlayerName();
                note.set(x + 2, "P:" + ChatColor.RED + score.getPoints() + ChatColor.WHITE + "K:" + ChatColor.AQUA + score.getKills() + ChatColor.WHITE + "D:" + ChatColor.DARK_PURPLE + score.getDeath() + "A:" + ChatColor.GREEN + score.getAssists() + ChatColor.GOLD + pName);
            }
        }
    }

    @Override
    protected Note setNoteText(Note note) {
        int current = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        int timeRemaining = startTime + length - current;
        note.addVar("time", ChatColor.RED + "Time Remaining: " + ChatColor.GREEN + "{" + timeRemaining + "s}");
        note.addVar("status", ChatColor.RED + "Status: " + ChatColor.BLUE + "{" + status.toString() + "}");
        return note;
    }

}
