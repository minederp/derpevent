/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.derpevent.event;

import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 *
 * @author thronecth
 */
public interface IEvent {

    public void playerDied(Player player, EntityDamageEvent cause);

    public void playerJoined(Player player);

//    public void playerKilledPlayer(Player killer, Player killed);

    public void playerAssistedKill(Player killer, Player killed, List<Player> assistors);

    public boolean okToEnd();

    public boolean okToStart();

    public void playerDied(Player player, EntityDamageEvent cause, boolean forciblyRemove);

}
