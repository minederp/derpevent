/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.event;

import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.EventLastManStanding;
import me.ctharvey.derpevent.event.EventSpleef;
import me.ctharvey.derpevent.event.EventTimed;
import me.ctharvey.derpevent.event.EventType;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import me.spathwalker.realms.zone.Zone;
import me.spathwalker.realms.zone.ZoneType;
import me.spathwalker.realms.zone.Zones;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class SubCommandCreate extends SubCommand {

    public SubCommandCreate(Command command) {
        super(command, "create", SubCommandType.PLAYER, "derpevent.event.create");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        Event event = null;
        String havenName;
        int length = -1;
        switch (args.length) {
            case 0:
            case 1:
                DerpEvent.send(cs, "You must use following format: /event create <type> <name> <havenname>.");
                return;
            case 2:
                havenName = null;
                break;
            case 3:
                havenName = args[2];
                break;
            case 4:
                havenName = args[2];
                if (!StringUtils.isNumeric(args[3])) {
                    DerpEvent.send(cs, "You must provide a number for time length.");
                    return;
                }
                length = Integer.parseInt(args[3]);
                break;
            default:
                DerpEvent.send(cs, "You provided too many arguments.");
                DerpEvent.send(cs, "You must use following format: /event create <type> <name> <havenname>.");
                return;
        }

        EventType type = EventType.get(args[0]);
        if (type == null) {
            DerpEvent.send(cs, "You have provided an invalid type.  Valid types include: " + EventType.getStringList());
            return;
        }
        if (type.requiresHaven() && havenName == null) {
            DerpEvent.send(cs, "This event type requires a haven name.");
        }
        String eventName = StringUtils.capitalize(args[1].toLowerCase());
        Zone zone;
        int zoneID = -1;
        if (type.requiresHaven()) {
            zoneID = Zones.get(args[2], ZoneType.Haven);
        }
        if (!type.requiresHaven() || zoneID != -1) {
            zone = Zones.load(zoneID);
            World world = ((Player) cs).getWorld();
            switch (type) {
                case SINGLE_LIFE:
                    event = new EventLastManStanding((Player) cs, eventName, world, zone);
                    break;
                case TIMED:
                    event = new EventTimed((Player) cs, eventName, world, zone);
                    if (length != -1) {
                        ((EventTimed) event).setLength(length);
                    }
                    break;
                case LAST_MAN_STANDING:
                    event = new EventLastManStanding((Player) cs, eventName, world, zone);
                    break;
                case SPLEEF:
                    event = new EventSpleef((Player) cs, eventName, world, zone);
                case TEAM:
                    break;
            }
            try {
                EventController.addNewEvent(event);
            } catch (Exception ex) {
                Logger.getLogger(SubCommandCreate.class.getName()).log(Level.SEVERE, null, ex);
                DerpEvent.send(cs, "Error: " + ex.toString());
                DerpEvent.send(cs, "There is an event already running with this name.");
            }
        } else {
            DerpEvent.send(cs, "Unable to locate haven with name " + args[1]);
        }
    }

}
