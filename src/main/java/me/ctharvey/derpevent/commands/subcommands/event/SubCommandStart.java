/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.event;

import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.IEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class SubCommandStart extends SubCommand {

    public SubCommandStart(Command command) {
        super(command, "start", SubCommandType.ALL, "derpevents.event.start");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 0:
                DerpEvent.send((Player) cs, "You must specify which event to start. /event start <name>");
                break;
            case 1:
                String eventName = StringUtils.capitalize(args[0].toLowerCase());
                if (EventController.getEventIndex().containsKey(eventName)) {
                    Event event = EventController.getEventIndex().get(eventName);
                    if (((IEvent) event).okToStart()) {
                        event.startEvent();
                    } else {
                        DerpEvent.send((Player) cs, "Unable to start event named " + event.getName());
                    }
                } else {
                    DerpEvent.send((Player) cs, "Unable to locate event named " + eventName);
                }
                break;
        }
    }

}
