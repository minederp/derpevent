/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.event;

import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class SubCommandList extends SubCommand {

    public SubCommandList(Command command) {
        super(command, "list", SubCommandType.ALL, "derpevent.event.list");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        DerpEvent.send((Player) cs, EventController.getEventIndex().keySet().toString());
    }

}
