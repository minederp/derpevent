/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.eventcontainer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventContainerFactory;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import me.ctharvey.mdbase.menus.Menu;
import me.ctharvey.mdbase.menus.MenuHandler;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory.InvalidQuormObjectException;
import me.ctharvey.quormdata.structures.QuormObject;
import me.spathwalker.realms.zone.Worlds;
import me.spathwalker.realms.zone.Zone;
import me.spathwalker.realms.zone.ZoneType;
import me.spathwalker.realms.zone.Zones;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class SubCommandCreate extends SubCommand {

    public SubCommandCreate(Command command) {
        super(command, "create", SubCommandType.PLAYER, "derpevent.eventcontainer.create");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 0:
                DerpEvent.send(cs, "You must include a name for the event.");
                break;
            case 1:
                List<Integer> zonesInWorld = getZonesInWorld(((Player) cs).getWorld(), ZoneType.Haven);
                List<Zone> freeZones = getFreeZones(zonesInWorld);
                EventContainer newEvent = new EventContainer().setWorldID(Worlds.getID(((Player) cs).getWorld()));
                try {
                    newEvent.setName(args[0]);
                } catch (QuormObject.NameTooLongException ex) {
                    DerpEvent.send(cs, "Your name is too long!");
                    return;
                }
                if (freeZones.isEmpty()) {
                    DerpEvent.send(cs, "No free zones in world to choose from.  Create a new zone to get started.");
                } else {
                    createMenu(newEvent, freeZones, cs).show(((Player) cs));
                }
                break;
            default:
                DerpEvent.send(cs, "You have included too many arguments.");
        }
    }

    private List<Zone> getFreeZones(List<Integer> zonesInWorld) {
        List<Zone> freeZones = new ArrayList();
        for (int id : zonesInWorld) {
            Zone zone = Zones.load(id);
            HashMap<Integer, Integer> zoneToEventContainerIndex = EventContainerFactory.getInstance().getZoneToEventContainerIndex();
            if (!zoneToEventContainerIndex.containsKey(zone.id)) {
                freeZones.add(zone);
            }
        }
        return freeZones;
    }

    private Menu createMenu(EventContainer event, List<Zone> freeZones, CommandSender cs) {
        SelectPlayZoneMenuHandler selectPlayZoneMenuHandler = new SelectPlayZoneMenuHandler(cs, event, freeZones);
        Menu menu = new Menu("Select Play Zone", selectPlayZoneMenuHandler);
        selectPlayZoneMenuHandler.setMenu(menu);
        for (Zone zone : freeZones) {
            menu.add(new ItemStack(Material.RAILS), zone.getName() + "(" + zone.id + ")", new String[0]);
        }
        return menu;
    }

    private class SelectPlayZoneMenuHandler extends DerpEventMenuHandler {

        private final List<Zone> freeZones;

        public SelectPlayZoneMenuHandler(CommandSender cs, EventContainer ec, List<Zone> freeZones) {
            super(cs, ec);
            this.freeZones = freeZones;
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Result.DENY) {
                return Result.DENY;
            }
            Zone get = this.freeZones.get(ice.getRawSlot());
            this.getEventContainer().setPlayZoneID(get.id);
            List<Zone> fFreeZones = getFreeZones(getZonesInWorld(((Player) cs).getWorld(), ZoneType.Haven));
            Menu menu = new Menu("Select Lobby Zone", new SelectLobbyZoneMenuHandler(cs, getEventContainer(), fFreeZones));
            for (Zone zone : fFreeZones) {
                menu.add(new ItemStack(Material.RAILS), zone.getName(), new String[0]);
            }
            menu.show((Player) cs);
            ice.getView().close();
            return Event.Result.DENY;
        }

    }

    private class SelectLobbyZoneMenuHandler extends DerpEventMenuHandler {

        private final List<Zone> freezones;

        public SelectLobbyZoneMenuHandler(CommandSender cs, EventContainer ec, List<Zone> freeZones) {
            super(cs, ec);
            this.freezones = freeZones;
        }

        @Override
        public Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            try {
                if (super.onClick(ice, bln, bln1) == Result.DENY) {
                    return Result.DENY;
                }
                Zone zone = this.freezones.get(ice.getRawSlot());
                getEventContainer().setLobbyZoneID(zone.id);
                EventContainerFactory.getInstance().add(getEventContainer());

                ice.getView().close();
            } catch (SQLException ex) {
                Logger.getLogger(SubCommandCreate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidQuormObjectException ex) {
                Logger.getLogger(SubCommandCreate.class.getName()).log(Level.SEVERE, null, ex);
            }
            return Event.Result.DENY;

        }

    }

    public static class DerpEventMenuHandler extends MenuHandler {

        public DerpEventMenuHandler(CommandSender cs, EventContainer ec) {
            super(cs, ec, null);
        }

        public EventContainer getEventContainer() {
            return (EventContainer) this.object;
        }
    }

    public static List<Integer> getZonesInWorld(World world, ZoneType type) {
        List<Integer> zoneIds = new ArrayList();
        int id = Worlds.getID(world);
        ResultSet rs = Zones.table.select("ID", "World = " + id + " AND Type = '" + type.chr + "'");
        try {
            if (rs != null && rs.isBeforeFirst()) {
                while (rs.next()) {
                    zoneIds.add(rs.getInt(1));
                }
            }
        } catch (SQLException ex) {
        }
        return zoneIds;

    }
}
