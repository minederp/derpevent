/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.event;

import java.util.Map.Entry;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.IEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class SubCommandLeave extends SubCommand {

    public SubCommandLeave(Command command) {
        super(command, "leave", SubCommandType.ALL, "derpevent.event.leave");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        if (!EventController.getEventIndex().isEmpty()) {
            for (Entry<String, Event> entry : EventController.getEventIndex().entrySet()) {
                if (entry.getValue().getPlayers().contains((Player) cs)) {
                    ((IEvent) entry.getValue()).playerDied((Player) cs, null, true);
                    DerpEvent.send((Player) cs, "You have left the event named: &c" + entry.getValue().getName());
                    return;
                }
            }
        }
        DerpEvent.send((Player) cs, "You are not currently in an event.");
    }

}
