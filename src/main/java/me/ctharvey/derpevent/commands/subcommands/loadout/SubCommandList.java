/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.loadout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventContainerFactory;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import me.ctharvey.derpevent.eventcontainer.Loadout;
import me.ctharvey.derpevent.eventcontainer.Loadout.EquipmentType;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import me.ctharvey.mdbase.menus.Menu;
import me.ctharvey.mdbase.menus.MenuHandler;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class SubCommandList extends SubCommand {

    public SubCommandList(Command command) {
        super(command, "list", SubCommandType.PLAYER, "derpevent.loadout.list");
    }

    private void showContainerMenu(CommandSender cs) {
        Menu menu = new Menu("Select a container.", new SelectEventContainerMenuHandler(cs, null));
        for (EventContainer ec : EventContainerFactory.getInstance().getIndex().values()) {
            List<String> lore = new ArrayList();
            EventVariation[] toArray = ec.getEventVariations().values().toArray(new EventVariation[0]);
            for (EventVariation ev : toArray) {
                lore.add(ev.getName() + " " + ev.getEventType());
            }
            menu.add(new ItemStack(Material.APPLE), ec.getName(), lore.toArray(new String[0]));
        }
        menu.show((Player) cs);
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        EventContainer containerFromLocation = EventContainerFactory.getInstance().getContainerFromLocation((Player) cs);
        if (containerFromLocation == null) {
            showContainerMenu(cs);
            return;
        }
        showVariationsMenu(cs, containerFromLocation);
    }

    private class SelectEventContainerMenuHandler extends MenuHandler {

        public SelectEventContainerMenuHandler(CommandSender cs, Object object) {
            super(cs, object, null);
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Event.Result.DENY;
            }
            String name = ice.getCurrentItem().getItemMeta().getDisplayName().substring(2);
            EventContainer current = null;
            for (EventContainer ec : EventContainerFactory.getInstance().getIndex().values()) {
                if (ec.getName().toLowerCase().equals(name.toLowerCase())) {
                    current = ec;
                    break;
                }
            }
            if (current != null) {
                showVariationsMenu(cs, current);
            }
            ice.getView().close();
            return Event.Result.DENY;
        }

    }

    public void showVariationsMenu(CommandSender cs, EventContainer containerFromLocation) {
        HashMap<String, EventVariation> eventVariations = containerFromLocation.getEventVariations();
        if (eventVariations.isEmpty()) {
            DerpEvent.send(cs, "There are no current variations for this container.  Try adding one with /eventcontainer add <name>.");
            return;
        }
        Menu menu = new Menu("Select variant to view loadouts for.", new StartMenuHandler(cs, containerFromLocation));
        for (Map.Entry<String, EventVariation> entry : eventVariations.entrySet()) {
            menu.add(new ItemStack(Material.APPLE), entry.getKey(), entry.getValue().getLore());
        }
        menu.show((Player) cs);
    }

    public class StartMenuHandler extends MenuHandler {

        public StartMenuHandler(CommandSender cs, EventContainer ec) {
            super(cs, ec, null);
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Event.Result.DENY;
            }
            String name = ice.getCurrentItem().getItemMeta().getDisplayName().substring(2);
            EventContainer ec = (EventContainer) this.object;
            if (ec.getEventVariations().containsKey(name)) {
                EventVariation ev = ec.getEventVariations().get(name);
                showLoadoutMenu(cs, ev);
            } else {
                DerpEvent.send(cs, "You have selected an invalid variation.");
            }
            ice.getView().close();
            return Event.Result.DENY;
        }

    }

    private void showLoadoutMenu(CommandSender cs, EventVariation ev) {
        Menu menu = new Menu("Select a loadout for more info.", new LoadoutMenuHandler(cs, ev));
        List<Loadout> loadouts = ev.getEquipmentOptions().getLoadouts();
        int cnt = 1;
        for (Loadout loadout : loadouts) {
            String name;
            if (loadout.getName() == null) {
                name = "Loadout #" + cnt;
            } else {
                name = loadout.getName();
            }
            menu.add(new ItemStack(Material.IRON_SWORD), name, loadout.getLore());

            cnt++;
        }
        menu.show((Player) cs);
    }

    public static class LoadoutMenuHandler extends MenuHandler {

        public LoadoutMenuHandler(CommandSender cs, Object object) {
            super(cs, object, null);
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Result.DENY;
            }
            int rawSlot = ice.getRawSlot();
            if (((EventVariation) object).getEquipmentOptions().getLoadouts().size() > 0) {
                Loadout loadout = ((EventVariation) object).getEquipmentOptions().getLoadouts().get(rawSlot);
                Menu menu = new Menu("Specific loadout info.", new SpecificLoadoutMenuHandler(cs, loadout));
                for (EquipmentType type : EquipmentType.values()) {
                    ItemStack item = loadout.getItem(type);
                    switch (type) {
                        case OTHERITEMS:
                            menu.add(new ItemStack(Material.CHEST), loadout.getOtherItems().size() + " other Items(Click to see)", getLore(loadout, type).toArray(new String[0]));
                            break;
                        default:
                            if (item != null) {
                                menu.add(loadout.getItem(type), getName(item), getLore(loadout, type).toArray(new String[0]));
                            } else {
                                menu.add(new ItemStack(Material.AIR), null, new String[0]);
                            }
                            break;
                    }
                }
                menu.show((Player) cs);
                ice.getView().close();
            }
            return Result.DENY;
        }
    }

    public static String getName(Loadout loadout, EquipmentType type) {
        ItemStack item = loadout.getItem(type);
        return getName(item);
    }

    public static String getName(ItemStack item) {
        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
            return item.getItemMeta().getDisplayName();
        }
        return item.getType().name();
    }

    public static List<String> getLore(Loadout loadout, EquipmentType type) {
        if (type.equals(EquipmentType.OTHERITEMS)) {
            List<String> lore = new ArrayList();
            List<ItemStack> otherItems = loadout.getOtherItems();
            for (ItemStack item : otherItems) {
                if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
                    lore.add(item.getItemMeta().getDisplayName());
                } else {
                    lore.add(item.getType().name());
                }
            }
            return lore;
        }
        ItemStack item = loadout.getItem(type);
        if (item.hasItemMeta() && item.getItemMeta().hasLore()) {
            return item.getItemMeta().getLore();
        }
        return new ArrayList();
    }

    private static class SpecificLoadoutMenuHandler extends MenuHandler {

        public SpecificLoadoutMenuHandler(CommandSender cs, Object object) {
            super(cs, object, null);
        }

        @Override
        public Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Result.DENY) {
                return Result.DENY;
            }
            int rawslot = ice.getRawSlot();
            EquipmentType type = EquipmentType.values()[rawslot];
            if (type == EquipmentType.OTHERITEMS) {
                ice.getView().close();
                Menu menu = new Menu("Current contents of chest.", new MenuHandler(cs, object, null) {
                });
                Loadout loadout = (Loadout) object;
                for (ItemStack item : loadout.getOtherItems()) {
                    menu.add(item, getName(loadout, EquipmentType.WEAPON), getLore(loadout, EquipmentType.WEAPON).toArray(new String[0]));
                }
                menu.show((Player) cs);
            }
            return Result.DENY;
        }

    }

}
