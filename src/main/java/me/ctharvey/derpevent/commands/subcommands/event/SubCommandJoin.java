/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.event;

import java.util.List;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import static me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandList.getLore;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import me.ctharvey.derpevent.eventcontainer.Loadout;
import me.ctharvey.derpevent.eventcontainer.Loadout.EquipmentType;
import me.ctharvey.derpevent.eventlisting.EventListing;
import me.ctharvey.derpevent.eventlisting.EventListingFactory;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import me.ctharvey.mdbase.menus.Menu;
import me.ctharvey.mdbase.menus.MenuHandler;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class SubCommandJoin extends SubCommand {

    private boolean whitelist = false;

    public SubCommandJoin(Command command) {
        super(command, "join", SubCommandType.PLAYER, "derpevent.event.join");
    }

    private void showLoadoutMenu(CommandSender cs, Event event) {
        Menu menu = new Menu("Select a loadout for more info.", new LoadoutMenuHandler(cs, event));
        List<Loadout> loadouts = event.getEquipment().getLoadouts();
        int cnt = 1;
        for (Loadout loadout : loadouts) {
            String name;
            if (loadout.getName() == null) {
                name = "Loadout #" + cnt;
            } else {
                name = loadout.getName();
            }
            menu.add(new ItemStack(Material.IRON_SWORD), name, loadout.getLore());

            cnt++;
        }
        menu.show((Player) cs);
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 0:
                //TODO show list of current events.
                DerpEvent.send(cs, "The sign did not specify which event to join.");
                break;
            case 1:
                String eventName = StringUtils.capitalize(args[0].toLowerCase());
                Event event = EventController.getEvent(eventName);
                if (event != null) {
                    EventVariation.EquipmentOptions equipment = event.getEquipment();
                    showLoadoutMenu(cs, event);
                } else {
                    try {
                        EventListing listing = EventListingFactory.getInstance().getFromName(eventName);
                        listing.sendTo(cs);
                    } catch (Exception ex) {
                        DerpEvent.send(cs, "You have specified an invalid event.");
                    }
                }
                break;
            case 2:
                String name = args[1];
                Player player = Bukkit.getPlayer(name);
                eventName = StringUtils.capitalize(args[0].toLowerCase());
                event = EventController.getEvent(eventName);
                if (event != null) {
                    if (player != null) {
                        if (cs.hasPermission("derpevent.event.subcommandjoin.player")) {
                            whitelist = true;
                            showLoadoutMenu(player, event);
                        }
                    } else {
                        DerpEvent.send(cs, "You have specified an invalid player.");
                    }
                } else {
                    DerpEvent.send(cs, "You have specified an invalid event.");
                }
                break;
            default:
                break;
        }
    }

    public class LoadoutMenuHandler extends EventJoinHandler {

        public LoadoutMenuHandler(CommandSender cs, Event event) {
            super(cs, event);
        }

        @Override
        public org.bukkit.event.Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == org.bukkit.event.Event.Result.DENY) {
                return org.bukkit.event.Event.Result.DENY;
            }
            int rawSlot = ice.getRawSlot();
            if (getEvent().getEquipment().getLoadouts().size() > 0) {
                Loadout loadout = getEvent().getEquipment().getLoadouts().get(rawSlot);
                Menu menu = new Menu("Specific loadout info.", new SpecificLoadoutMenuHandler(cs, getEvent(), loadout));
                for (Loadout.EquipmentType type : Loadout.EquipmentType.values()) {
                    ItemStack item = loadout.getItem(type);
                    switch (type) {
                        case OTHERITEMS:
                            menu.add(new ItemStack(Material.CHEST), loadout.getOtherItems().size() + " other Items(Click to see)", getLore(loadout, type).toArray(new String[0]));
                            break;
                        default:
                            if (item != null) {
                                menu.add(loadout.getItem(type), me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandList.getName(item), getLore(loadout, type).toArray(new String[0]));
                            } else {
                                menu.add(new ItemStack(Material.AIR), null, new String[0]);
                            }
                            break;
                    }
                }
                menu.add(new ItemStack(Material.COMPASS), "Go back", new String[0]);
                menu.add(new ItemStack(Material.SLIME_BALL), "Select", new String[0]);
                menu.useCancelButton();
                menu.show((Player) cs);
                ice.getView().close();
            }
            return org.bukkit.event.Event.Result.DENY;
        }
    }

    private class SpecificLoadoutMenuHandler extends EventJoinHandler {

        private final Loadout loadout;

        public SpecificLoadoutMenuHandler(CommandSender cs, Event event, Loadout loadout) {
            super(cs, event);
            this.loadout = loadout;
        }

        @Override
        public Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == org.bukkit.event.Event.Result.DENY) {
                return org.bukkit.event.Event.Result.DENY;
            }
            int rawslot = ice.getRawSlot();
            if (ice.getCurrentItem().getType().equals(Material.COMPASS) && ice.getCurrentItem().getItemMeta().getDisplayName().toLowerCase().contains("back")) {
                showLoadoutMenu(cs, getEvent());
                ice.getView().close();
                return Result.DENY;
            }
            if (ice.getCurrentItem().getType().equals(Material.SLIME_BALL) && ice.getCurrentItem().getItemMeta().getDisplayName().toLowerCase().contains("select")) {
                selectLoadout((Player) cs, getEvent(), loadout);
                ice.getView().close();
                return Result.DENY;
            }
            if (rawslot < EquipmentType.values().length) {
                Loadout.EquipmentType type = Loadout.EquipmentType.values()[rawslot];
                if (type == Loadout.EquipmentType.OTHERITEMS && loadout.getOtherItems().size() > 0) {
                    ice.getView().close();
                    Menu menu = new Menu("Other items in loadout", new MenuHandler(cs, getEvent(), null) {

                        @Override
                        public Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
                            if (super.onClick(ice, bln, bln1) == Result.DENY) {
                                return Result.DENY;
                            }
                            if (ice.getCurrentItem().getType().equals(Material.SLIME_BALL) && ice.getCurrentItem().getItemMeta().getDisplayName().toLowerCase().contains("select")) {
                                selectLoadout((Player) cs, getEvent(), loadout);
                                ice.getView().close();
                                return Result.DENY;
                            }
                            Menu menu = new Menu("Specific loadout info.", new SpecificLoadoutMenuHandler(cs, getEvent(), loadout));
                            for (Loadout.EquipmentType type : Loadout.EquipmentType.values()) {
                                ItemStack item = loadout.getItem(type);
                                switch (type) {
                                    case OTHERITEMS:
                                        menu.add(new ItemStack(Material.CHEST), loadout.getOtherItems().size() + " other Items(Click to see)", getLore(loadout, type).toArray(new String[0]));
                                        break;
                                    default:
                                        if (item != null) {
                                            menu.add(loadout.getItem(type), me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandList.getName(item), getLore(loadout, type).toArray(new String[0]));
                                        } else {
                                            menu.add(new ItemStack(Material.AIR), null, new String[0]);
                                        }
                                        break;
                                }
                            }
                            menu.add(new ItemStack(Material.COMPASS), "Go back", new String[0]);
                            menu.add(new ItemStack(Material.SLIME_BALL), "Select", new String[0]);
                            menu.useCancelButton();
                            menu.show((Player) cs);
                            ice.getView().close();
                            return Result.DENY;
                        }

                    });
                    for (ItemStack item : loadout.getOtherItems()) {
                        menu.add(item, me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandList.getName(loadout, Loadout.EquipmentType.WEAPON), getLore(loadout, Loadout.EquipmentType.WEAPON).toArray(new String[0]));
                    }
                    menu.add(new ItemStack(Material.COMPASS), "Go back", new String[0]);
                    menu.useCancelButton();
                    menu.show((Player) cs);
                }
            }
            return Result.DENY;
        }

        private void selectLoadout(Player player, Event event, Loadout loadout) {
            if (whitelist && event.isWhitelistOn()) {
                event.addPlayer(player, true);
                loadout.setupEquipment(player);
            } else if (!whitelist && event.isWhitelistOn()) {
                DerpEvent.send((CommandSender) player, "You are not whitelisted for this event.");
            } else {
                event.addPlayer(player, false);
                loadout.setupEquipment(player);
            }
        }

    }

    public abstract class EventJoinHandler extends MenuHandler {

        public EventJoinHandler(CommandSender cs, Event event) {
            super(cs, event, null);
        }

        public Event getEvent() {
            return (Event) object;
        }

    }
}
