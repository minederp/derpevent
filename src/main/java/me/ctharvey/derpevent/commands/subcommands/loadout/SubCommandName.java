/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.loadout;

import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import org.bukkit.command.CommandSender;

/**
 *
 * @author thronecth
 */
public class SubCommandName extends SubCommand {

    public SubCommandName(Command command) {
        super(command, "name", SubCommandType.PLAYER, "derpevent.loadout.name");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 0:
                DerpEvent.send(cs, "You have not provided the name you want to name a loadout.");
                break;
            default:
                break;

        }
    }

}
