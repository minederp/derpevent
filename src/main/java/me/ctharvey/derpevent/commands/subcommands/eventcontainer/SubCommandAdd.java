/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.eventcontainer;

import java.util.Map;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.commands.subcommands.eventcontainer.SubCommandCreate.DerpEventMenuHandler;
import me.ctharvey.derpevent.event.EventType;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventContainerFactory;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import me.ctharvey.derpevent.eventcontainer.options.Option;
import me.ctharvey.derpevent.eventcontainer.options.OptionType;
import me.ctharvey.derpevent.eventcontainer.options.OptionsMenu;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import me.ctharvey.mdbase.menus.Menu;
import me.spathwalker.realms.zone.Zone;
import me.spathwalker.realms.zone.ZoneType;
import me.spathwalker.realms.zone.Zones;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class SubCommandAdd extends SubCommand {

    public SubCommandAdd(Command command) {
        super(command, "add", SubCommandType.PLAYER, "derpevent.eventcontainer.add");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 0:
                DerpEvent.send(cs, "You must provide a name for the variant.");
                break;
            case 1:
                String variantName = args[0];
                Block block = ((Player) cs).getLocation().getBlock();
                int zoneID = Zones.get(block);
                if (zoneID == -1) {
                    sendOutOfZoneMessage(cs);
                    return;
                }
                Zone eventZone = Zones.load(zoneID);
                if (eventZone.getType() != ZoneType.Haven) {
                    eventZone = eventZone.getParent();
                }
                EventContainer container;
                if (!EventContainerFactory.getInstance().getZoneToEventContainerIndex().containsKey(eventZone.id)) {
                    sendOutOfZoneMessage(cs);
                    return;
                } else {
                    container = EventContainerFactory.getInstance().get(EventContainerFactory.getInstance().getZoneToEventContainerIndex().get(eventZone.id));
                }
                Menu menu = new Menu(null, new VariantCreateMenuHandler(cs, container, variantName));
                for (EventType type : EventType.values()) {
                    menu.add(new ItemStack(type.getIcon()), type.name(), type.getLore());
                }
                menu.show(((Player) cs));
                break;
            default:
                DerpEvent.send(cs, "You provided too many arguements");
        }
    }

    private void sendOutOfZoneMessage(CommandSender cs) {
        DerpEvent.send(cs, "You are not in any valid event zones.  Make sure to be in a container for an event to add a variant.");
    }

    public class VariantCreateMenuHandler extends DerpEventMenuHandler {

        private final String variantName;

        public VariantCreateMenuHandler(CommandSender cs, EventContainer ec, String variantName) {
            super(cs, ec);
            this.variantName = variantName;
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Result.DENY) {
                return Result.DENY;
            }
            EventType type = EventType.values()[ice.getRawSlot()];
            EventVariation ev = new EventVariation(variantName, type, getEventContainer());
            Map<OptionType, Option> toggleableOptions = ev.getOptions().getToggleableOptions();
            OptionsMenu menu = new OptionsMenu(new OptionsMenu.OptionsMenuHandler(cs, getEventContainer(), ev));
            menu.show((Player) cs);
            return Result.DENY;
        }

    }

}
