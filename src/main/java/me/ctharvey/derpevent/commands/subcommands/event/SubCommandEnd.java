/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.event;

import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import org.bukkit.command.CommandSender;

/**
 *
 * @author thronecth
 */
public class SubCommandEnd extends SubCommand {

    public SubCommandEnd(Command command) {
        super(command, "end", SubCommandType.ALL, "derpevent.event.end");
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 0:
                DerpEvent.send(cs, "You must specify which event to end.");
                return;
            case 1:
                String eventName = StringUtils.capitalize(args[0].toLowerCase());
                if (EventController.getEvent(eventName) != null) {
                    Event event = EventController.getEvent(eventName);
                    event.endEvent();
                    break;
                } else {
                    DerpEvent.send(cs, "Unable to locate event with name " + args[0] + ".");
                }
        }
    }

}
