/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands.subcommands.loadout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventContainerFactory;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import me.ctharvey.derpevent.eventcontainer.Loadout;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.command.SubCommand;
import me.ctharvey.mdbase.menus.Menu;
import me.ctharvey.mdbase.menus.MenuHandler;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class SubCommandAdd extends SubCommand {

    private String loadoutName;

    public SubCommandAdd(Command command) {
        super(command, "add", SubCommandType.PLAYER, "derpevent.loadout.add");
    }

    private void showContainerMenu(CommandSender cs) {
        Menu menu = new Menu("Select a container.", new SelectEventContainerMenuHandler(cs, null));
        for (EventContainer ec : EventContainerFactory.getInstance().getIndex().values()) {
            List<String> lore = new ArrayList();
            EventVariation[] toArray = ec.getEventVariations().values().toArray(new EventVariation[0]);
            for (EventVariation ev : toArray) {
                lore.add(ev.getName() + " " + ev.getEventType());
            }
            menu.add(new ItemStack(Material.APPLE), ec.getName(), lore.toArray(new String[0]));
        }
        menu.show((Player) cs);
    }

    public void showVariationsMenu(CommandSender cs, EventContainer containerFromLocation) {
        HashMap<String, EventVariation> eventVariations = containerFromLocation.getEventVariations();
        if (eventVariations.isEmpty()) {
            DerpEvent.send(cs, "There are no current variations for this container.  Try adding one with /eventcontainer add <name>.");
            return;
        }
        Menu menu = new Menu("Select variant to start.", new StartMenuHandler(cs, containerFromLocation));
        for (Map.Entry<String, EventVariation> entry : eventVariations.entrySet()) {
            menu.add(new ItemStack(Material.APPLE), entry.getKey(), entry.getValue().getLore());
        }
        menu.show((Player) cs);
    }

    @Override
    protected void runSubCommand(CommandSender cs, String[] args) {
        switch (args.length) {
            case 1:
                this.loadoutName = args[0];
            case 0:
                EventContainer containerFromLocation = EventContainerFactory.getInstance().getContainerFromLocation((Player) cs);
                if (containerFromLocation == null) {
                    showContainerMenu(cs);
                    return;
                }
                showVariationsMenu(cs, containerFromLocation);
                break;
            default:
                break;
        }
    }

    private class SelectEventContainerMenuHandler extends MenuHandler {

        public SelectEventContainerMenuHandler(CommandSender cs, Object object) {
            super(cs, object, null);
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Event.Result.DENY;
            }
            String name = ice.getCurrentItem().getItemMeta().getDisplayName().substring(2);
            EventContainer current = null;
            for (EventContainer ec : EventContainerFactory.getInstance().getIndex().values()) {
                if (ec.getName().toLowerCase().equals(name.toLowerCase())) {
                    current = ec;
                    break;
                }
            }
            if (current != null) {
                showVariationsMenu(cs, current);
            }
            return Event.Result.DENY;
        }

    }

    public class StartMenuHandler extends MenuHandler {

        public StartMenuHandler(CommandSender cs, EventContainer ec) {
            super(cs, ec, null);
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Event.Result.DENY;
            }
            String name = ice.getCurrentItem().getItemMeta().getDisplayName().substring(2);
            EventContainer ec = (EventContainer) this.object;
            if (ec.getEventVariations().containsKey(name)) {
                EventVariation ev = ec.getEventVariations().get(name);
                Loadout loadout = new Loadout(ev.getEquipmentOptions());
                if (name != null) {
                    loadout.setName(name);
                }
                ev.getEquipmentOptions().addLoadout(loadout);
                ev.update();
                Loadout.LoadoutMenu menu = new Loadout.LoadoutMenu(Loadout.EquipmentType.HELMET, cs, loadout, false);
            } else {
                DerpEvent.send(cs, "You have selected an invalid variation.");
            }
            return Event.Result.DENY;
        }

    }

}
