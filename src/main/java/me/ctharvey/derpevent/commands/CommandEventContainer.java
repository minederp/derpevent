/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.commands.subcommands.eventcontainer.SubCommandAdd;
import me.ctharvey.derpevent.commands.subcommands.eventcontainer.SubCommandCreate;
import me.ctharvey.derpevent.commands.subcommands.eventcontainer.SubCommandRemove;
import me.ctharvey.derpevent.commands.subcommands.eventcontainer.SubCommandStart;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventContainerFactory;
import me.ctharvey.mdbase.command.SubCommand;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author thronecth
 */
public class CommandEventContainer extends me.ctharvey.mdbase.command.Command implements CommandExecutor {

    public CommandEventContainer(MDBasePlugin plugin) {
        super(plugin, "eventcontainer");
        addSubCommand(new SubCommandCreate(this));
        addSubCommand(new SubCommandAdd(this));
        addSubCommand(new SubCommandList(this));
        addSubCommand(new SubCommandStart(this));
        addSubCommand(new SubCommandRemove(this));
    }

    public static class SubCommandList extends SubCommand {

        public SubCommandList(me.ctharvey.mdbase.command.Command command) {
            super(command, "list", SubCommandType.PLAYER, "derp.eventcontainer.list");
        }

        @Override
        protected void runSubCommand(CommandSender cs, String[] args) {
            for (EventContainer ec : EventContainerFactory.getInstance().getIndex().values()) {
                DerpEvent.send(cs, ec.toString());
            }
        }
    }

}
