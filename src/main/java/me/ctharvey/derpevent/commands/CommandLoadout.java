/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands;

import me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandAdd;
import me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandList;
import me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandRemove;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.command.CommandExecutor;

/**
 *
 * @author thronecth
 */
public class CommandLoadout extends me.ctharvey.mdbase.command.Command implements CommandExecutor {

    public CommandLoadout(MDBasePlugin plugin) {
        super(plugin, "loadout");
        addSubCommand(new SubCommandAdd(this));
        addSubCommand(new SubCommandList(this));
        addSubCommand(new SubCommandRemove(this));
    }

}
