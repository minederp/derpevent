/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.commands;

import me.ctharvey.derpevent.commands.subcommands.event.SubCommandLeave;
import java.util.Arrays;
import java.util.List;
import me.ctharvey.derpevent.commands.subcommands.event.SubCommandCreate;
import me.ctharvey.derpevent.commands.subcommands.event.SubCommandEnd;
import me.ctharvey.derpevent.commands.subcommands.event.SubCommandJoin;
import me.ctharvey.derpevent.commands.subcommands.event.SubCommandList;
import me.ctharvey.derpevent.commands.subcommands.event.SubCommandStart;
import me.ctharvey.pluginbase.MDBasePlugin;
import org.bukkit.command.CommandExecutor;

/**
 *
 * @author thronecth
 */
public class CommandEvent extends me.ctharvey.mdbase.command.Command implements CommandExecutor {

    public CommandEvent(MDBasePlugin plugin) {
        super(plugin, "event");
        addSubCommand(new SubCommandJoin(this));
        addSubCommand(new SubCommandLeave(this));
        addSubCommand(new SubCommandStart(this));
        addSubCommand(new SubCommandCreate(this));
        addSubCommand(new SubCommandEnd(this));
        addSubCommand(new SubCommandList(this));
    }

    @Override
    public List<String> getCommandNames() {
        return Arrays.asList("event", "e");
    }

}
