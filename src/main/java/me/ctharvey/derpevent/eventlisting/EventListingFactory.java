/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventlisting;

import com.adamantdreamer.quorm.core.DAO;
import java.util.Map;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory;

/**
 *
 * @author thronecth
 */
public class EventListingFactory extends QuormUniqueNameObjectFactory<EventListing> {

    private static EventListingFactory instance;

    public EventListingFactory(QuormHandler dao) {
        super(dao, EventListing.class);
        EventListingFactory.instance = this;
    }

    @Override
    public Map<Integer, EventListing> getIndex() {
        return this.index;
    }

    public static EventListingFactory getInstance() {
        return instance;
    }

    @Override
    public EventListing get(int id) {
        return (EventListing) super.get(id); //To change body of generated methods, choose Tools | Templates.
    }

}
