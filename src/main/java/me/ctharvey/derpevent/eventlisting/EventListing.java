/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventlisting;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Var;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.event.Event.Status;
import me.ctharvey.derpevent.event.EventType;
import me.ctharvey.mdbase.teleport.TeleporterInterface;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.multiserv.packet.Packet;
import me.ctharvey.multiserv.packet.Packet.PacketType;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory.InvalidQuormObjectException;
import me.ctharvey.quormdata.structures.QuormObject;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;

/**
 *
 * @author thronecth
 */
public class EventListing extends QuormObject.QuormUniqueIDObject.QuormUniqueNameObject {

    @Var
    @Size(32)
    private String serverName;
    private int type;
    private List<String> currentPlayers = new ArrayList<>();
    private Status status;

    public EventListing(Status status, String eventName, String serverName, EventType type) {
        this.status = status;
        this.name = eventName;
        this.type = type.ordinal();
        this.serverName = serverName;
    }

    public EventListing() {
    }

    public void addPlayer(Player player) {
        this.currentPlayers.add(player.getName());
    }

    public void removePlayer(Player player) {
        this.currentPlayers.remove(player.getName());
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public EventType getType() {
        return EventType.values()[type];
    }

    @Override
    public Class getClassType() {
        return this.getClass();
    }

    @Override
    public boolean update() {
        try {
            return EventListingFactory.getInstance().update(this);
        } catch (SQLException | InvalidQuormObjectException ex) {
            Logger.getLogger(EventListing.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete() {
        try {
            return EventListingFactory.getInstance().delete(this);
        } catch (SQLException | InvalidQuormObjectException ex) {
            Logger.getLogger(EventListing.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public String getEventName() {
        return this.getName();
    }

    public String getServerName() {
        return serverName;
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void sendTo(CommandSender cs) {
        try {
            PlayerID id = PlayerIDS.get((Player) cs);
            DerpEvent.getPlugin().getCurrentTeleporter().teleportPlayerToServer(id, serverName);
            MultiServeClient.sendPacket(new EventListingPacket(serverName, Bukkit.getServerName(), PacketType.GENERAL, new EventListingPayload(id.getId(), this.id)));
        } catch (TeleporterInterface.InvalidCrossServerTP | SQLException ex) {
            Logger.getLogger(EventListing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public class EventListingPacket extends Packet<EventListingPayload> {

        public EventListingPacket() {
        }

        public EventListingPacket(String sendToServer, String sendFromServer, PacketType type, EventListingPayload payload) {
            super(EventListingPacket.class, sendToServer, sendFromServer, type, payload);
        }

        @Override
        public EventListingPayload getData() {
            EventListingPayload payload = (EventListingPayload) super.getStoredData();
            return payload;
        }

        public EventListing getEventListing() {
            return EventListingFactory.getInstance().get(getData().getEventListing());
        }

        public String getPlayerName() throws SQLException {
            return getData().getPlayerID().getName();
        }

        @Override
        public void processData(JSONObject jo2) {
            EventListingPayload eventListingPayload = new EventListingPayload();
            eventListingPayload.fromJSONObject(jo2);
            this.setData(eventListingPayload);
        }

    }
}
