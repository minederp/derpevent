/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventlisting;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import me.ctharvey.multiserv.packet.JSONable;
import org.json.simple.JSONObject;

/**
 *
 * @author thronecth
 */
public class EventListingPayload implements JSONable {

    public EventListingPayload() {
    }

    private int playerID;
    private int eventListing;

    public EventListingPayload(int playerID, int eventListing) {
        this.playerID = playerID;
        this.eventListing = eventListing;
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        jo.put("playerid", playerID);
        jo.put("eventListing", eventListing);
        return jo;
    }

    @Override
    public void fromJSONObject(JSONObject jo) {
        playerID = (int) (long) jo.get("playerid");
        eventListing = (int) (long) jo.get("eventListing");
    }

    @Override
    public void fromString(String string) {
    }

    public PlayerID getPlayerID() throws SQLException {
        return PlayerIDS.get(playerID);
    }

    public int getEventListing() {
        return eventListing;
    }

}
