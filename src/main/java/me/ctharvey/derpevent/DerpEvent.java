/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.commands.CommandEvent;
import me.ctharvey.derpevent.commands.CommandEventContainer;
import me.ctharvey.derpevent.commands.CommandLoadout;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.IEvent;
import me.ctharvey.derpevent.eventlisting.EventListing;
import me.ctharvey.derpevent.eventlisting.EventListingFactory;
import me.ctharvey.mdbase.MDDatabasePlugin;
import me.ctharvey.mdbase.chat.LocalSend;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class DerpEvent extends MDDatabasePlugin {

    private static DerpEvent plugin;

    private List<CommandInterface> commands = new ArrayList();

    public DerpEvent() {
        super("DerpEvent", ChatColor.DARK_AQUA, "[", "]", new LocalSend());
    }

    public static DerpEvent getPlugin() {
        return plugin;
    }

    @Override
    public List<CommandInterface> getCommands() {
        return commands;
    }

    @Override
    public void startup() {
        DerpEvent.plugin = this;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

            @Override
            public void run() {
                for (Entry<String, Event> entry : EventController.getEventIndex().entrySet()) {
                    IEvent event = (IEvent) entry.getValue();
                    if (entry.getValue().getStatus().equals(Event.Status.RUNNING) && event.okToEnd()) {
                        ((Event) event).endEvent();
                    } else {
                        ((Event) event).updateScoreBoard();
                    }
                }
            }
        }, 0, 20);
        this.initDB();
        commands.add(new CommandEvent(this));
        commands.add(new CommandEventContainer(this));
        commands.add(new CommandLoadout(this));
        Bukkit.getPluginManager().registerEvents(new EventController(this.getDbHandler()), plugin);
    }

    public static void send(Player player, String msg) {
        plugin.sendMsg(msg, player);
    }

    public static void send(CommandSender cs, String msg) {
        send((Player) cs, msg);
    }

    @Override
    public void shutdown() {
        for (Iterator<Entry<Integer, EventListing>> it = EventListingFactory.getInstance().getIndex().entrySet().iterator(); it.hasNext();) {
            try {
                Entry<Integer, EventListing> entry = it.next();
                EventListingFactory.getInstance().delete(entry.getValue());
                it.remove();
            } catch (SQLException | QuormUniqueNameObjectFactory.InvalidQuormObjectException ex) {
                Logger.getLogger(DerpEvent.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
