/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer;

import com.adamantdreamer.quorm.define.Ignore;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory.InvalidQuormObjectException;
import me.ctharvey.quormdata.structures.QuormObject.QuormUniqueIDObject.QuormUniqueNameObject;
import me.spathwalker.realms.zone.Worlds;
import me.spathwalker.realms.zone.Zone;
import me.spathwalker.realms.zone.Zones;
import org.bukkit.World;

/**
 *
 * @author thronecth
 */
public class EventContainer extends QuormUniqueNameObject implements Serializable {

    @Ignore
    public static final long serialVersionUID = -5998863704020966136l;

    private int playZoneID = -1, lobbyZoneID = -1, worldID = -1;

    private AutoRunEvent autoRunEvent = null;

    private final HashMap<String, EventVariation> eventVariations = new HashMap<>();

    public EventContainer() {
        super();
    }

    public EventContainer(int id) {
        this.id = id;
    }

    public Zone getPlayZone() {
        return Zones.load(playZoneID);
    }

    public Zone getLobbyZone() {
        return Zones.load(lobbyZoneID);
    }

    public World getWorld() {
        return Worlds.get(id);
    }

    public void setPlayZoneID(int playZoneID) {
        this.playZoneID = playZoneID;
    }

    public void setLobbyZoneID(int lobbyZoneID) {
        this.lobbyZoneID = lobbyZoneID;
    }

    public EventContainer setWorldID(int worldID) {
        this.worldID = worldID;
        return this;
    }

    public AutoRunEvent getAutoRunEvent() {
        return autoRunEvent;
    }

    public void setAutoRunEvent(AutoRunEvent autoRunEvent) {
        this.autoRunEvent = autoRunEvent;
    }

    public void addVariation(EventVariation variation) throws SQLException {
        try {
            this.eventVariations.put(variation.getName(), variation);
            EventContainerFactory.getInstance().update(this);
        } catch (InvalidQuormObjectException ex) {
            Logger.getLogger(EventContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getWorldID() {
        return worldID;
    }

    @Override
    public String toString() {
        return this.getName() + " : " + this.getId() + " : " + this.getLobbyZone().getName() + " : " + this.getPlayZone().getName() + " : " + this.getWorldID();
    }

    public HashMap<String, EventVariation> getEventVariations() {
        return eventVariations;
    }

    @Override
    public Class getClassType() {
        return this.getClass();
    }

    @Override
    public boolean update() {
        try {
            return EventContainerFactory.getInstance().update(this);
        } catch (SQLException | InvalidQuormObjectException ex) {
            Logger.getLogger(EventContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete() {
        try {
            return EventContainerFactory.getInstance().delete(this);
        } catch (SQLException | InvalidQuormObjectException ex) {
            Logger.getLogger(EventContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public class AutoRunEvent implements Serializable {

        private boolean autoRun = false;
        private EventVariation eventToRun;
        private int minimumNeededToStart = 5;
        private int timeToStart = 60;

        public boolean isAutoRun() {
            return autoRun;
        }

        public void setAutoRun(boolean autoRun) {
            this.autoRun = autoRun;
        }

        public EventVariation getEventToRun() {
            return eventToRun;
        }

        public void setEventToRun(EventVariation eventToRun) {
            this.eventToRun = eventToRun;
        }

        public int getMinimumNeededToStart() {
            return minimumNeededToStart;
        }

        public void setMinimumNeededToStart(int minimumNeededToStart) {
            this.minimumNeededToStart = minimumNeededToStart;
        }

        public int getTimeToStart() {
            return timeToStart;
        }

        public void setTimeToStart(int timeToStart) {
            this.timeToStart = timeToStart;
        }

    }
}
