/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.EventController;
import me.ctharvey.derpevent.commands.subcommands.event.SubCommandCreate;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.EventType;
import me.ctharvey.derpevent.event.points.MonsterPoint;
import me.ctharvey.derpevent.event.points.SpawnPoint;
import me.ctharvey.derpevent.event.points.TreasurePoint;
import me.ctharvey.derpevent.eventcontainer.options.Option;
import me.ctharvey.derpevent.eventcontainer.options.OptionType;
import me.ctharvey.derpevent.eventcontainer.options.Options;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public class EventVariation implements Serializable {

    private final String name;

    private final Options options;

    private final List<SpawnPoint> spawnPoints = new ArrayList();
    private final List<MonsterPoint> monsterPoints = new ArrayList();
    private final List<TreasurePoint> treasurePoints = new ArrayList();

    private EventType type;
    private final EventContainer eventContainer;

    private final EquipmentOptions eOptions = new EquipmentOptions(this);

    public EventVariation(String name, EventType type, EventContainer eventContainer) {
        this.name = name;
        this.type = type;
        this.options = new Options(type);
        this.eventContainer = eventContainer;
    }

    public void setOptions(Event event) throws OptionType.OptionInvalidEventException, OptionType.OptionInvalidException, OptionType.OptionInvalidTypeException {
        options.set(event);
    }

    public Options getOptions() {
        return options;
    }

    public void addSpawnPoint(SpawnPoint point) {
        this.spawnPoints.add(point);
    }

    public void addMonsterPoint(MonsterPoint point) {
        this.monsterPoints.add(point);
    }

    public void addTreasurePoint(TreasurePoint point) {
        this.treasurePoints.add(point);
    }

    public List<SpawnPoint> getSpawnPoints() {
        return spawnPoints;
    }

    public List<MonsterPoint> getMonsterPoints() {
        return monsterPoints;
    }

    public List<TreasurePoint> getTreasurePoints() {
        return treasurePoints;
    }

    public String getName() {
        return name;
    }

    public void createNewEvent(Player owner, String name, World world) {
        try {
            Event event = getEventType().createType(owner, name, world, eventContainer.getPlayZone());
            event.setEquipment(eOptions);
            this.setOptions(event);
            try {
                EventController.addNewEvent(event);
            } catch (Exception ex) {
                Logger.getLogger(SubCommandCreate.class.getName()).log(Level.SEVERE, null, ex);
                DerpEvent.send(owner, "Error: " + ex.toString());
                DerpEvent.send(owner, "There is an event already running with this name.");
            }
        } catch (OptionType.OptionInvalidEventException | OptionType.OptionInvalidException | OptionType.OptionInvalidTypeException ex) {
            Logger.getLogger(EventContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public EventType getEventType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public String[] getLore() {
        List<String> lore = new ArrayList();
        lore.add(this.getEventType().name());
        for (Option option : getOptions().getOptions()) {
            lore.add(option.getOptionType().name() + " = " + option.getValue().toString());
        }
        return lore.toArray(new String[0]);
    }

    public void update() {
        this.getEventContainer().update();
    }

    private EventContainer getEventContainer() {
        return this.eventContainer;
    }

    public static class EquipmentOptions implements Serializable {

        private EventVariation parentVariation;

        public EquipmentOptions(EventVariation parentVariation) {
            this.parentVariation = parentVariation;
        }

        List<Loadout> loadouts = new ArrayList();

        public void addLoadout(Loadout loadout) {
            this.loadouts.add(loadout);
        }

        public void removeLoadout(Loadout loadout) {
            this.loadouts.remove(loadout);
        }

        public EventVariation getParentVariation() {
            return parentVariation;
        }

        public List<Loadout> getLoadouts() {
            return this.loadouts;
        }

    }

    public EquipmentOptions getEquipmentOptions() {
        return eOptions;
    }

}
