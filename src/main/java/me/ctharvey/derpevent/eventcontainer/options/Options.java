/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer.options;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.EventType;

/**
 *
 * @author thronecth
 */
public class Options implements Serializable {

    private final LinkedHashMap<OptionType, Option> toggleableOptions = new LinkedHashMap<>();
    private final List<Option> options = new ArrayList();

    public Options(EventType type) {
        for (Option option : getToggleables(type)) {
            toggleableOptions.put(option.getOptionType(), option);
            options.add(option);
        }
    }

    public static Option[] getToggleables(EventType type) {
        Set<Option> options = new HashSet();
        options.add(OptionType.WHITELIST.getOption().setValue(false));
        options.add(OptionType.MOBS.getOption().setValue(false));
        options.add(OptionType.ANIMALS.getOption().setValue(false));
        options.add(OptionType.MOVEBEFOREEVENT.getOption().setValue(false));
        switch (type) {
            case LAST_MAN_STANDING:
            case SINGLE_LIFE:
                options.addAll(Arrays.asList(
                        OptionType.PVPALLOWED.getOption().setValue(true)));
                break;
            case SPLEEF:
                options.addAll(Arrays.asList(
                        OptionType.PVPALLOWED.getOption().setValue(false),
                        OptionType.SPLEEFBLOCKPLACE.getOption().setValue(false)));
                break;
            case TIMED:
                options.addAll(Arrays.asList(
                        OptionType.PVPALLOWED.getOption().setValue(true),
                        OptionType.TIMEREMAINING.getOption().setValue(60)));
                break;
        }
        return options.toArray(new Option[0]);
    }

    public void set(Event event) throws OptionType.OptionInvalidEventException, OptionType.OptionInvalidException, OptionType.OptionInvalidTypeException {
        for (Entry<OptionType, Option> entry : toggleableOptions.entrySet()) {
            entry.getKey().set(event, entry.getValue());
        }
    }

    public LinkedHashMap<OptionType, Option> getToggleableOptions() {
        return toggleableOptions;
    }

    public List<Option> getOptions() {
        return options;
    }

}
