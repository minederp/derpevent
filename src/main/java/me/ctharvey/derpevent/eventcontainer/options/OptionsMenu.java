/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer.options;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import me.ctharvey.mdbase.menus.Menu;
import me.ctharvey.mdbase.menus.MenuHandler;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class OptionsMenu extends Menu {

    public OptionsMenu(OptionsMenuHandler handler) {
        super("Select Options", handler);
        handler.setMenu(this);
        for (Option option : handler.getEv().getOptions().getOptions()) {
            this.add(new ItemStack(option.getOptionType().getLogo()), option.getOptionType().name(), option.getValue().toString());
        }
        this.useCancelButton();
        this.useCompleteButton();
    }

    public static class OptionsMenuHandler extends MenuHandler {

        private final EventVariation ev;

        public OptionsMenuHandler(CommandSender cs, EventContainer ec, EventVariation ev) {
            super(cs, ec, new OptionsMenuCompleter(ec, ev));
            this.ev = ev;
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Event.Result.DENY;
            }

            int slot = ice.getRawSlot();
            if (slot <= ev.getOptions().getOptions().size()) {
                Option option = ev.getOptions().getOptions().get(slot);
                option.executeClick(ice, cs, (EventContainer) object, ev);
                ice.getView().close();
            }
            return Event.Result.DENY;
        }

        public EventVariation getEv() {
            return ev;
        }

    }

    private static class OptionsMenuCompleter implements MenuHandler.MenuCompleteInterface {

        private final EventVariation ev;
        private final EventContainer ec;

        public OptionsMenuCompleter(EventContainer ec, EventVariation ev) {
            this.ev = ev;
            this.ec = ec;
        }

        @Override
        public void complete() {
            try {
                this.ec.addVariation(ev);
            } catch (SQLException ex) {
                Logger.getLogger(OptionsMenu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
