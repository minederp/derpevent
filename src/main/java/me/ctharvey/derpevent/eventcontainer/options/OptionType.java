/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer.options;

import java.io.Serializable;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.EventSpleef;
import me.ctharvey.derpevent.event.EventTimed;
import me.ctharvey.derpevent.event.EventType;
import me.ctharvey.derpevent.eventcontainer.options.Option.BooleanOption;
import me.ctharvey.derpevent.eventcontainer.options.Option.IntegerOption;
import org.bukkit.Material;

/**
 *
 * @author thronecth
 */
public enum OptionType implements Serializable {

    SPLEEFBLOCKPLACE(BooleanOption.class, Material.SNOW_BLOCK) {
                @Override
                public Option getOption() {
                    return new Option.BooleanOption(this, Boolean.TRUE);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    if (!event.getType().equals(EventType.SPLEEF)) {
                        throw new OptionInvalidEventException(event, option);
                    }
                    EventSpleef e = (EventSpleef) event;
                    Boolean placeBlocks = ((BooleanOption) option).getValue();
                    if (placeBlocks) {
                        e.addAllowedToPlaceBlocks(Material.SNOW);
                    } else {
                        e.removeAllowedToPlaceBlock(Material.SNOW);
                    }
                }
            },
    PVPALLOWED(BooleanOption.class, Material.IRON_SWORD) {
                @Override
                public Option getOption() {
                    return new Option.BooleanOption(this, Boolean.TRUE);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    event.setPvpAllowed((boolean) option.getValue());
                }

            },
    MOVEBEFOREEVENT(BooleanOption.class, Material.IRON_BOOTS) {
                @Override
                public Option getOption() {
                    return new Option.BooleanOption(this, Boolean.TRUE);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    event.setMoveBeforeEvent((boolean) option.getValue());
                }

            },
    MOBS(BooleanOption.class, Material.MOB_SPAWNER) {
                @Override
                public Option getOption() {
                    return new Option.BooleanOption(this, Boolean.TRUE);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    event.setSpawnMobs((boolean) option.getValue());
                }

            },
    ANIMALS(BooleanOption.class, Material.PORK) {
                @Override
                public Option getOption() {
                    return new Option.BooleanOption(this, Boolean.TRUE);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    event.setSpawnAnimals((boolean) option.getValue());
                }

            },
    WHITELIST(BooleanOption.class, Material.BOOK_AND_QUILL) {
                @Override
                public Option getOption() {
                    return new Option.BooleanOption(this, Boolean.TRUE);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    event.setWhitelistOn((boolean) option.getValue());
                }

            },
    TIMEREMAINING(IntegerOption.class, Material.COMPASS) {

                @Override
                public Option getOption() {
                    return new Option.IntegerOption(this, 60);
                }

                @Override
                public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
                    super.set(event, option);
                    if (!(event instanceof EventTimed)) {
                        throw new OptionInvalidEventException(event, option);

                    }
                    EventTimed e = (EventTimed) event;
                    e.setLength((int) option.getValue());
                }

            };

    private final Class clazzType;
    private final Material logo;

    private OptionType(Class clazzType, Material logo) {
        this.clazzType = clazzType;
        this.logo = logo;
    }

    public abstract Option getOption();

    public void set(Event event, Option option) throws OptionInvalidEventException, OptionInvalidException, OptionInvalidTypeException {
        if (!option.getOptionType().equals(this)) {
            throw new OptionInvalidException(this, option);
        }
        if (option.getClass() != this.clazzType) {
            throw new OptionInvalidTypeException(this.clazzType, option.getClass());
        }
    }

    public Material getLogo() {
        return logo;
    }

    public static class OptionInvalidEventException extends Exception {

        public OptionInvalidEventException(Event event, Option option) {
            super("The event " + event.getName() + " is " + event.getType().toString() + " type. You are trying to set an option for " + option.getOptionType().toString() + " which is invalid.");
        }
    }

    public static class OptionInvalidException extends Exception {

        public OptionInvalidException(OptionType aThis, Option option) {
            super("The option " + option.getOptionType().toString() + " does not match the type needed: " + aThis.name());
        }
    }

    public static class OptionInvalidTypeException extends Exception {

        public OptionInvalidTypeException(Class clazzType, Class<? extends Option> aClass) {
            super("This requires an option type of " + clazzType.getName() + " but you supplied " + aClass.getName());
        }
    }

}
