/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer.options;

import java.io.Serializable;
import me.ctharvey.derpevent.eventcontainer.EventContainer;
import me.ctharvey.derpevent.eventcontainer.EventVariation;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author thronecth
 */
public abstract class Option implements Serializable {

    private static long serialVersionUID = 396086342870218576l;

    private final OptionType option;
    private Object value;

    public Option(OptionType type, Object value) {
        this.option = type;
        this.value = value;
    }

    public OptionType getOptionType() {
        return option;
    }

    public Object getValue() {
        return value;
    }

    public Option setValue(Object value) {
        this.value = value;
        return this;
    }

    public abstract void executeClick(InventoryClickEvent ice, CommandSender cs, EventContainer eventContainer, EventVariation ev);

    public static class BooleanOption extends Option {

        public BooleanOption(OptionType type, Boolean value) {
            super(type, value);
        }

        @Override
        public Boolean getValue() {
            return (Boolean) super.getValue();
        }

        @Override
        public void executeClick(InventoryClickEvent ice, CommandSender cs, EventContainer eventContainer, EventVariation ev) {
            this.setValue(!this.getValue());
            OptionsMenu menu = new OptionsMenu(new OptionsMenu.OptionsMenuHandler(cs, eventContainer, ev));
            menu.show((Player) cs);
        }
    }

    public static class IntegerOption extends Option {

        public IntegerOption(OptionType type, int value) {
            super(type, value);
        }

        @Override
        public Integer getValue() {
            return (int) super.getValue();
        }

        @Override
        public void executeClick(InventoryClickEvent ice, CommandSender cs, EventContainer eventContainer, EventVariation ev) {
            ClickType type = ice.getClick();
            int changeAmount = 0;
            switch (type) {
                case LEFT:
                    changeAmount = 1;
                    break;
                case RIGHT:
                    changeAmount = -1;
                    break;
                case NUMBER_KEY:
                    int hotbarButton = ice.getHotbarButton();
                    changeAmount = hotbarButton * 10 + 1;
                    break;
                case SHIFT_LEFT:
                    changeAmount = 10;
                    break;
                case SHIFT_RIGHT:
                    changeAmount = -10;
                    break;
            }
            this.setValue(this.getValue() + changeAmount);
            OptionsMenu menu = new OptionsMenu(new OptionsMenu.OptionsMenuHandler(cs, eventContainer, ev));
            menu.show((Player) cs);
        }
    }
}
