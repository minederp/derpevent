/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.derpevent.DerpEvent;
import me.ctharvey.derpevent.commands.subcommands.loadout.SubCommandList;
import me.ctharvey.mdbase.menus.Menu;
import me.ctharvey.mdbase.menus.MenuHandler;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

/**
 *
 * @author thronecth
 */
public class Loadout implements Serializable {

    private static final long serialVersionUID = 2581934363103867234l;
    private ItemStack weapon, helmet, chest, leggings, boots;
    private List<ItemStack> otherItems = new ArrayList();
    private String name;

    private EventVariation.EquipmentOptions parentOptions;

    public Loadout(EventVariation.EquipmentOptions parentOptions) {
        this.parentOptions = parentOptions;
    }

    public ItemStack getWeapon() {
        if (weapon == null) {
            return new ItemStack(Material.AIR);
        }
        return weapon;
    }

    public void setWeapon(ItemStack weapon) {
        setWeapon(weapon, 0F);
    }

    public void setWeapon(ItemStack weapon, float percent) {
        this.weapon = weapon;
    }

    public ItemStack getHelmet() {
        if (helmet == null) {
            return new ItemStack(Material.AIR);
        }
        return helmet;
    }

    public void setHelmet(ItemStack helmet) {
        setHelmet(helmet, 0F);
    }

    public void setHelmet(ItemStack helmet, float percent) {
        this.helmet = helmet;
    }

    public ItemStack getChest() {
        if (chest == null) {
            return new ItemStack(Material.AIR);
        }
        return chest;
    }

    public void setChest(ItemStack chest) {
        setChest(chest, 0F);
    }

    public void setChest(ItemStack chest, float percent) {
        this.chest = chest;
    }

    public ItemStack getLeggings() {
        if (leggings == null) {
            return new ItemStack(Material.AIR);
        }
        return leggings;
    }

    public void setLeggings(ItemStack leggings) {
        setLeggings(leggings, 0F);
    }

    public void setLeggings(ItemStack leggings, float percent) {
        this.leggings = leggings;
    }

    public ItemStack getBoots() {
        if (boots == null) {
            return new ItemStack(Material.AIR);
        }
        return boots;
    }

    public void setBoots(ItemStack boots) {
        setBoots(boots, 0F);
    }

    public void setBoots(ItemStack boots, float percent) {
        this.boots = boots;
    }

    public void setupEquipment(Player player) {
        player.getEquipment().setItemInHand(setNamedItem(weapon, EquipmentType.WEAPON));
        player.getEquipment().setBoots(setNamedItem(boots, EquipmentType.BOOTS));
        player.getEquipment().setChestplate(setNamedItem(chest, EquipmentType.CHEST));
        player.getEquipment().setHelmet(setNamedItem(helmet, EquipmentType.HELMET));
        player.getEquipment().setLeggings(setNamedItem(leggings, EquipmentType.LEGGINGS));
    }

    public ItemStack setNamedItem(ItemStack equip, EquipmentType type) {
        if (equip != null && equip.getType() != Material.AIR) {
            equip = equip.clone();
            ItemMeta meta = equip.getItemMeta();
            if (!equip.getItemMeta().hasDisplayName()) {
                String name = equip.getType().toString().toLowerCase().replace('_', ' ');
                meta.setDisplayName(name);
            }
            List<String> lore = equip.getItemMeta().getLore();
            List<String> newLore = new ArrayList();
            if (lore != null) {
                newLore.addAll(lore);
            }
            meta.setLore(newLore);
            equip.setItemMeta(meta);
        }
        return equip;
    }

    public boolean hasEquipment() {
        for (EquipmentType type : EquipmentType.values()) {
            if (getItem(type) != null && !getItem(type).getType().equals(Material.AIR)) {
                return true;
            }
        }
        return false;
    }

    public ItemStack getItem(EquipmentType type) {
        ItemStack item = null;
        switch (type) {
            case BOOTS:
                item = boots;
                break;
            case CHEST:
                item = chest;
                break;
            case HELMET:
                item = helmet;
                break;
            case LEGGINGS:
                item = leggings;
                break;
            case WEAPON:
                item = weapon;
                break;
        }
        return item;
    }

    private EventVariation.EquipmentOptions getParentOptions() {
        return this.parentOptions;
    }

    public String[] getLore() {
        List<String> lore = new ArrayList();
        for (EquipmentType type : EquipmentType.values()) {
            boolean enchanted = false;
            if (type != EquipmentType.OTHERITEMS) {
                ItemStack item = getItem(type);
                if (item != null && !item.getType().equals(Material.AIR)) {
                    if (item.getEnchantments() != null && !item.getEnchantments().isEmpty()) {
                        enchanted = true;
                    }
                    String enchantmentString = "";
                    if (enchanted) {
                        enchantmentString = getEnchantmentString(item);
                    }
                    lore.add(type.name() + " : " + enchantmentString + " " + SubCommandList.getName(this, type));
                }
            } else {
                if (getOtherItems().size() > 0) {
                    lore.add("Other Items: " + getOtherItems().size());
                }
            }
        }
        return lore.toArray(new String[0]);
    }

    private String getEnchantmentString(ItemStack item) {
        StringBuilder sb = new StringBuilder();
        Map<Enchantment, Integer> enchantments = item.getEnchantments();
        for (Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
            sb.append(entry.getKey().getName().substring(0, 2)).append(entry.getValue());
        }
        return sb.toString();
    }

    public static enum EquipmentType implements Serializable {

        HELMET,
        WEAPON,
        CHEST,
        LEGGINGS,
        BOOTS,
        OTHERITEMS;

        public ItemStack getEItem(PlayerInventory equipment) {
            ItemStack item = null;
            switch (this) {
                case BOOTS:
                    item = equipment.getBoots();
                    break;
                case CHEST:
                    item = equipment.getChestplate();
                    break;
                case HELMET:
                    item = equipment.getHelmet();
                    break;
                case LEGGINGS:
                    item = equipment.getLeggings();
                    break;
                case WEAPON:
                    item = equipment.getItemInHand();
                    break;
            }
            return item;
        }
    }

    public static Loadout setItem(ItemStack item, EquipmentType type, Loadout equip) {
        if (item == null) {
            try {
                throw new Exception("Invalid item passed.");
            } catch (Exception ex) {
                Logger.getLogger(Loadout.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        switch (type) {
            case BOOTS:
                equip.setBoots(item);
                break;
            case CHEST:
                equip.setChest(item);
                break;
            case HELMET:
                equip.setHelmet(item);
                break;
            case LEGGINGS:
                equip.setLeggings(item);
                break;
            case WEAPON:
                equip.setWeapon(item);
                break;
        }
        return equip;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        BukkitObjectOutputStream oout = new BukkitObjectOutputStream(out);
        oout.writeObject(this.boots);
        oout.writeObject(this.chest);
        oout.writeObject(this.helmet);
        oout.writeObject(this.leggings);
        oout.writeObject(this.weapon);
        oout.writeObject(this.otherItems);
        oout.writeObject("END");
    }

    private void readObject(ObjectInputStream in) throws IOException {
        try {
            BukkitObjectInputStream iin = new BukkitObjectInputStream(in);
            this.boots = (ItemStack) iin.readObject();
            this.chest = (ItemStack) iin.readObject();
            this.helmet = (ItemStack) iin.readObject();
            this.leggings = (ItemStack) iin.readObject();
            this.weapon = (ItemStack) iin.readObject();
            this.otherItems = (List<ItemStack>) iin.readObject();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Loadout.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ItemStack> getOtherItems() {
        return otherItems;
    }

    public void addOtherItems(ItemStack addItem) {
        this.otherItems.add(addItem);
    }

    public static class LoadoutMenu extends Menu {

        public LoadoutMenu(EquipmentType type, CommandSender cs, Loadout equip, boolean singleItem) {
            super("Select " + type.toString(), new LoadoutMenuhandler(type, cs, equip, singleItem));
            this.useCancelButton();
            List<ItemStack> otherItems;
            ItemStack item;
            switch (type) {
                case OTHERITEMS:
                    otherItems = equip.getOtherItems();
                    for (ItemStack otherItemsItem : otherItems) {
                        if (otherItemsItem.getItemMeta() != null && otherItemsItem.getItemMeta().hasLore()) {
                            this.add(otherItemsItem, null, otherItemsItem.getItemMeta().getLore().toArray(new String[0]));
                        } else {
                            this.add(otherItemsItem, null, new String[0]);
                        }
                    }
                    break;
                default:
                    item = equip.getItem(type);
                    if (item != null) {
                        if (item.getItemMeta() != null && item.getItemMeta().hasLore()) {
                            this.add(item, null, item.getItemMeta().getLore().toArray(new String[0]));
                        } else {
                            this.add(item, null, new String[0]);
                        }
                    } else {
                        this.add(new ItemStack(Material.AIR), null, new String[0]);
                    }
                    break;
            }
            this.add(new ItemStack(Material.ANVIL), "Continue", new String[0]);
            this.show((Player) cs);

        }

    }

    public static class LoadoutMenuhandler extends MenuHandler {

        private final EquipmentType type;
        private boolean singleItem;

        public LoadoutMenuhandler(EquipmentType type, CommandSender cs, Loadout equip, boolean singleItem) {
            super(cs, equip, null);
            this.type = type;
            this.singleItem = singleItem;
        }

        @Override
        public Event.Result onClick(InventoryClickEvent ice, boolean bln, boolean bln1) {
            if (super.onClick(ice, bln, bln1) == Event.Result.DENY) {
                return Result.DENY;
            }
            Loadout equip = (Loadout) this.object;
            switch (type) {
                case OTHERITEMS:
                    return this.setOtherItems(ice, equip);
                default:
                    switch (id) {
                        case 0:
                            removeItem(ice, equip);
                            break;
                        case 1:
                            completeStep(ice, equip);
                            break;
                        default:
                            setCurrentItem(ice, equip);
                            break;
                    }
            }
            return Result.DENY;
        }

        private Result setOtherItems(InventoryClickEvent ice, Loadout equip) {
            ItemStack currentIItem = ice.getCurrentItem().clone();
            Inventory destInvent = ice.getInventory();
            Integer slotClicked = ice.getRawSlot();
            if (slotClicked < destInvent.getSize()) {
                // slot clicked was in the remote container, not in the player
                switch (currentIItem.getType()) {
                    case ANVIL:
                        completeStep(ice, equip);
                        return Result.DENY;
                    case AIR:
                        return Result.DENY;
                    default:
                        equip.getOtherItems().remove(ice.getRawSlot());
                }
                new LoadoutMenu(type, cs, equip, singleItem);
            } else {
                // slot clicked was in the player, not the remote container
                equip.getOtherItems().add(currentIItem);
                new LoadoutMenu(type, cs, equip, singleItem);
            }
            return Result.DENY;
        }

        private void removeItem(InventoryClickEvent ice, Loadout equip) {
            equip.setItem(new ItemStack(Material.AIR), type, equip);
            new LoadoutMenu(type, cs, equip, singleItem);
            ice.getView().close();
        }

        private void completeStep(InventoryClickEvent ice, Loadout equip) {
            if (singleItem) {
//                        new EnvironmentMenu.MobInfoMenu(cs, template, template);
            } else {
                if (type.ordinal() + 1 < EquipmentType.values().length) {
                    ice.getView().close();
                    new LoadoutMenu(EquipmentType.values()[type.ordinal() + 1], cs, equip, singleItem);
                } else {
                    ice.getView().close();
                    equip.getParentOptions().getParentVariation().update();
                    DerpEvent.send((Player) cs, "You have added a new loadout.  " + equip.getParentOptions().loadouts.size() + " current loadouts for this variant.");
                }
            }
        }

        private void setCurrentItem(InventoryClickEvent ice, Loadout loadout) {
            if (!ice.getCurrentItem().getType().equals(Material.AIR)) {
                ItemStack currentItem = ice.getCurrentItem().clone();
                currentItem.setAmount(1);
                Loadout.setItem(currentItem, type, loadout);
            }
            new LoadoutMenu(type, cs, loadout, singleItem);
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
