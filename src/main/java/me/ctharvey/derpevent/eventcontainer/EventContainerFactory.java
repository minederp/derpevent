/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.eventcontainer;

import com.adamantdreamer.quorm.core.DAO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory;
import me.ctharvey.quormdata.structures.QuormObject;
import me.spathwalker.realms.zone.Zone;
import me.spathwalker.realms.zone.ZoneType;
import me.spathwalker.realms.zone.Zones;
import org.bukkit.entity.Player;

/**
 *
 * @author thronecth
 */
public final class EventContainerFactory extends QuormUniqueNameObjectFactory<EventContainer> {

    private static EventContainerFactory instance;
    private final HashMap<Event, Integer> currentEventIndex = new HashMap();
    private final HashMap<Integer, Integer> zoneToEventContainerIndex = new HashMap();

    public EventContainerFactory(QuormHandler handler) {
        super(handler, EventContainer.class);
        EventContainerFactory.instance = this;
        init();
    }

    @Override
    public void init() {
        super.init();
        for (EventContainer cont : this.getIndex().values()) {
            if (cont.getPlayZone() != null) {
                zoneToEventContainerIndex.put(cont.getPlayZone().id, cont.getId());
            }
            if (cont.getLobbyZone() != null) {
                zoneToEventContainerIndex.put(cont.getLobbyZone().id, cont.getId());

            }
        }
    }

    public EventContainer getContainerFromEvent(Event event) {
        return (EventContainer) this.get(getCurrentEventIndex().get(event));
    }

    public HashMap<Event, Integer> getCurrentEventIndex() {
        return currentEventIndex;
    }

    public HashMap<Integer, Integer> getZoneToEventContainerIndex() {
        return zoneToEventContainerIndex;
    }

    public EventContainer getContainerFromLocation(Player player) {
        int zoneID = Zones.get(player.getLocation().getBlock());
        if (zoneID != -1) {
            Zone zone = Zones.load(zoneID);
            if (zone.getType() != ZoneType.Plot) {
                zoneID = zone.getParentID();
            }
            if (zoneToEventContainerIndex.containsKey(zoneID)) {
                return (EventContainer) this.get(zoneID);
            }
        }
        return null;
    }

    @Override
    public Map<Integer, EventContainer> getIndex() {
        return index;
    }

    public static EventContainerFactory getInstance() {
        return instance;
    }

    @Override
    public EventContainer get(int id) {
        return (EventContainer) super.get(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(QuormObject.QuormUniqueIDObject object) throws SQLException, InvalidQuormObjectException {
        if (!super.add(object)) {
            return false;
            //To change body of generated methods, choose Tools | Templates.
        }
        EventContainer cont = (EventContainer) object;
        if (cont.getPlayZone() != null) {
            zoneToEventContainerIndex.put(cont.getPlayZone().id, cont.getId());
        }
        if (cont.getLobbyZone() != null) {
            zoneToEventContainerIndex.put(cont.getLobbyZone().id, cont.getId());

        }
        return true;
    }

}
