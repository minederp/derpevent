/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent;

import com.adamantdreamer.foundation.core.event.PlayerBlockstepEvent;
import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import me.ctharvey.derpevent.event.Event;
import me.ctharvey.derpevent.event.Event.Status;
import me.ctharvey.derpevent.event.EventHavenBased;
import me.ctharvey.derpevent.event.EventType;
import me.ctharvey.derpevent.event.IEvent;
import me.ctharvey.derpevent.event.scoreboard.damagetracker.DamageTracker.DamageEntry;
import me.ctharvey.derpevent.eventcontainer.EventContainerFactory;
import me.ctharvey.derpevent.eventlisting.EventListingFactory;
import me.ctharvey.mdbase.database.QuormHandler;
import me.spathwalker.realms.zone.Box;
import me.spathwalker.realms.zone.Zone;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author thronecth
 */
public class EventController implements Listener {

    private static final HashMap<String, Event> eventIndex = new HashMap();
    private static Essentials essentials = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");

    public EventController(QuormHandler handler) {
        EventListingFactory listingFactory = new EventListingFactory(handler);
        EventContainerFactory containerFactory = new EventContainerFactory(handler);
    }

    public static Event getEvent(String eventName) {
        if (eventIndex.containsKey(eventName)) {
            return eventIndex.get(eventName);
        }
        return null;
    }

    public static void addNewEvent(Event event) throws Exception {
        if (eventIndex.containsKey(event.getName())) {
            throw new Exception("Event already exists.");
        }
        eventIndex.put(event.getName(), event);
        String createMsg = "A new event called " + ChatColor.RED + event.getName() + ChatColor.RESET + " has been created and will start soon. Visit /server Event or use /warp " + ChatColor.RED + event.getName() + ChatColor.RESET + " to join.";
        DerpEvent.getPlugin().broadcast(createMsg);
    }

    public static void removeEvent(Event event) {
        eventIndex.remove(event.getName());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity().getType().equals(EntityType.PLAYER)) {
            for (Entry<String, Event> entry : eventIndex.entrySet()) {
                Player damagee = (Player) e.getEntity();
                Event event = entry.getValue();
                if (event.getPlayers().contains(damagee)) {
                    if (event.getStatus().equals(Status.RUNNING)) {
                        if (damagee.getHealth() - e.getDamage() < 1) {
                            e.setCancelled(true);
                            ((IEvent) entry.getValue()).playerDied((Player) e.getEntity(), e.getEntity().getLastDamageCause());
                            entry.getValue().updateScoreBoard();
                            damagee.setHealth(damagee.getMaxHealth());
                        }
                    } else {
                        DerpEvent.getPlugin().sendMsg("You cannot be damaged anyone until the event starts.", damagee);
                        e.setCancelled(true);
                    }
                }

            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager().getType().equals(EntityType.PLAYER) && e.getEntity().getType().equals(EntityType.PLAYER)) {
            for (Entry<String, Event> entry : eventIndex.entrySet()) {
                Player damager = (Player) e.getDamager();
                Player damagee = (Player) e.getEntity();
                Event event = entry.getValue();
                if (event.getPlayers().contains(damager) || event.getPlayers().contains(damagee)) {
                    if (event.getPlayers().contains(damager) && event.getPlayers().contains(damagee)) {

                    } else {
                        e.setCancelled(true);
                    }
                } else if (event.getType().requiresHaven()) {
                    EventHavenBased eEvent = (EventHavenBased) event;
                    if (eEvent.getZone().box.contains(damagee.getLocation().getBlock())) {
                        e.setCancelled(true);
                    }
                }
                if (event.getPlayers().contains(damager) && event.getPlayers().contains(damagee)) {
                    if (!event.getStatus().equals(Event.Status.RUNNING)) {
                        EventType type = event.getType();
                        if (type.requiresHaven()) {
                            EventHavenBased eEvent = (EventHavenBased) event;
                            Zone zone = eEvent.getZone();
                            if (zone.box.contains(damager.getLocation().getBlock()) || zone.box.contains(damagee.getLocation().getBlock())) {
                                DerpEvent.getPlugin().sendMsg("You cannot damage by anyone until the event starts.", damager);
                                e.setCancelled(true);
                            }
                        }
                    } else {
                        event.getDamageTracker().addEntry(new DamageEntry((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), damager.getName(), damagee.getName()));
                        for (ItemStack item : damager.getEquipment().getArmorContents()) {
                            item.setDurability((short) 0);
                        }
                        if (damagee.getHealth() - e.getDamage() < 1) {
                            e.setCancelled(true);
                            ((IEvent) entry.getValue()).playerDied((Player) e.getEntity(), e.getEntity().getLastDamageCause());
                            entry.getValue().updateScoreBoard();
                            damagee.setHealth(damagee.getMaxHealth());
                        }
                    }
                } else {
                    DerpEvent.getPlugin().sendMsg("The player you're hitting is not in this event.", damager);
                    e.setCancelled(true);
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockChange(PlayerBlockstepEvent e) {
        for (Entry<String, Event> entry : eventIndex.entrySet()) {
            if (entry.getValue().getPlayers().contains(e.getPlayer()) && !entry.getValue().getStatus().equals(Status.RUNNING)) {
                DerpEvent.getPlugin().sendMsg("You cannot move until the event starts.", e.getPlayer());
                e.getPlayer().teleport(e.getFrom().getLocation());
            }
            if (entry.getValue().getType().requiresHaven()) {
                EventHavenBased event = (EventHavenBased) entry.getValue();
                User user = essentials.getUser(e.getPlayer());
                if (event.getZone().box.contains(e.getTo())) {
                    if (!event.getPlayers().contains(e.getPlayer()) && event.isAreaLocked() && !user.isVanished()) {
                        e.getPlayer().getEquipment().clear();
                        e.getPlayer().getInventory().clear();
                        user.setVanished(true);
                        DerpEvent.getPlugin().sendMsg("You were set to invisible while inside the event area.", e.getPlayer());
                    } else if (user.isVanished() && event.getPlayers().contains(e.getPlayer())) {
                        user.setVanished(false);
                    }
                } else {
                    if (event.getPlayers().contains(e.getPlayer())) {
                        e.getPlayer().getVelocity().multiply(-5);
                    } else if (user.isVanished()) {
                        user.setVanished(false);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDrop(PlayerDropItemEvent e) {
        for (Entry<String, Event> entry : eventIndex.entrySet()) {
            Event event = entry.getValue();
            if (event.getType().requiresHaven()) {
                EventHavenBased hvb = (EventHavenBased) event;
                Box box = hvb.getZone().box;
                if (box.contains(e.getPlayer().getLocation().getBlock())) {
                    e.setCancelled(true);
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent e) {
        for (Entry<String, Event> entry : eventIndex.entrySet()) {
            if (entry.getValue().getType().requiresHaven()) {
                EventHavenBased event = (EventHavenBased) entry.getValue();
                if (event.getZone().box.contains(e.getBlock()) && !event.getStatus().equals(Event.Status.RUNNING)) {
                    e.setCancelled(true);
                    DerpEvent.getPlugin().sendMsg("You cannot break blocks here while the event is waiting.", e.getPlayer());
                } else if (!event.getPlayers().contains(e.getPlayer()) || !event.allowsBlockBreak(e.getBlock().getType())) {
                    e.setCancelled(true);
                    DerpEvent.getPlugin().sendMsg("You cannot break this type of block.", e.getPlayer());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockPlace(BlockPlaceEvent e) {
        for (Entry<String, Event> entry : eventIndex.entrySet()) {
            if (entry.getValue().getType().requiresHaven()) {
                EventHavenBased event = (EventHavenBased) entry.getValue();
                if (event.getZone().box.contains(e.getBlock()) && !event.getStatus().equals(Event.Status.RUNNING)) {
                    e.setCancelled(true);
                    DerpEvent.getPlugin().sendMsg("You cannot place blocks here while the event is waiting.", e.getPlayer());
                } else if (!event.allowsBlockPlace(e.getBlock().getType())) {
                    e.setCancelled(true);
                    DerpEvent.getPlugin().sendMsg("You cannot place this type of block.", e.getPlayer());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onTeleport(PlayerTeleportEvent e) {
        for (Entry<String, Event> entry : eventIndex.entrySet()) {
            if (entry.getValue().getType().requiresHaven()) {
                EventHavenBased event = (EventHavenBased) entry.getValue();
                if (event.getPlayers().contains(e.getPlayer()) && !event.getZone().box.contains(e.getTo().getBlock())) {
                    IEvent iEvent = (IEvent) event;
                    iEvent.playerDied(e.getPlayer(), null, true);
                    e.getPlayer().getInventory().clear();
                    e.getPlayer().getInventory().setArmorContents(new ItemStack[e.getPlayer().getInventory().getArmorContents().length]);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onLeave(PlayerQuitEvent e) {
        for (Entry<String, Event> entry : eventIndex.entrySet()) {
            if (entry.getValue().getType().requiresHaven()) {
                EventHavenBased event = (EventHavenBased) entry.getValue();
                if (event.getPlayers().contains(e.getPlayer())) {
                    IEvent iEvent = (IEvent) event;
                    iEvent.playerDied(e.getPlayer(), null, true);
                    e.getPlayer().getInventory().clear();
                    e.getPlayer().getInventory().setArmorContents(new ItemStack[e.getPlayer().getInventory().getArmorContents().length]);
                }
            }
        }
    }

    public static HashMap<String, Event> getEventIndex() {
        return eventIndex;
    }

}
