/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.derpevent.event;

import java.util.Collections;
import me.ctharvey.derpevent.event.scoreboard.Score;
import me.ctharvey.derpevent.event.scoreboard.ScoreBoard;
import org.bukkit.entity.Player;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author thronecth
 */
public class EventTest {

    public EventTest() {
    }

    @Test
    public void testInitNewNote() {
    }

    @Test
    public void testGetStatus() {
    }

    @Test
    public void testGetType() {
    }

    @Test
    public void testMessagePlayers() {
    }

    @Test
    public void testUpdateScoreBoard() {
        ScoreBoard board = new ScoreBoard();
        board.addScore("test");
        board.addScore("alpha");
        board.addScore("bravo");
        board.getScores().get(1).addPoints(5);
        Collections.sort(board.getScores());
        for (Score score : board.getScores()) {
            System.out.print(score.getPlayerName() + " " + score.getPoints());
        }
        assert (board.getScores().get(0).getPoints() == 5);
    }

    @Test
    public void testGetName() {
    }

    @Test
    public void testSetName() {
    }

    @Test
    public void testGetPlayers() {
    }

    @Test
    public void testGetScore() {
    }

    @Test
    public void testGetWorld() {
    }

    @Test
    public void testSetWorld() {
    }

    @Test
    public void testEndEvent() {
    }

    @Test
    public void testGetScoreBoard() {
    }

    @Test
    public void testStartEvent() {
    }

    @Test
    public void testAddPoints() {
    }

    @Test
    public void testAddPlayer() {
    }

    @Test
    public void testRemovePoints() {
    }

    @Test
    public void testGetWinner() {
    }

    @Test
    public void testPlayerDied_3args() {
    }

    @Test
    public void testPlayerDied_Player_EntityDamageEvent() {
    }

}
